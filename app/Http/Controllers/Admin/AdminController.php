<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\AdminRoute;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\Activitylog\Models\Activity;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $admins = Admin::search($request->search, ['name', 'mobile']);

        if (env('APP_ENV') != 'local' && \Session::get('admin')->id != 1)
            $admins = $admins->where('id', '>', 1);

        return view('admin.manager.index', [
            'admins' => $admins->get()
        ]);
    }

    public function profile(Request $request)
    {
        if($request->isMethod('post')){

            $admin = Admin::find(\Session::get('admin')->id);

            $this->validate($request, [
                'name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10|unique:admins,mobile,'.$admin->id,
                'password' => 'min:6|nullable'
            ],[
                'name.required' => 'Name is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.unique' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'password.min' => 'Password length should be 6 or more',
            ]);

            $admin->name = $request->name;
            $admin->mobile = $request->mobile;
            $admin->password = !empty($request->password) ? \Hash::make($request->password) : $admin->password;
            $admin->save();

            \Session::put('admin', $admin);

            return redirect()->route('admin-dashboard')->with(['success' => 'Profile has been updated..!!']);
        }

        return view('admin.manager.profile');
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required',
                'mobile' => 'required|regex:/^[9876][0-9]{9}$/|digits:10|unique:admins,mobile',
                'email' => 'required|email|unique:admins,email',
                'password' => 'required|min:6',
                'routes' => 'required',
                'type' => 'required'
            ],[
                'name.required' => 'Admin Manager Name is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.unique' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'email.unique' => 'This Email Address is already exist',
                'password.required' => 'Password is required',
                'password.min' => 'Password length should be 6 or more',
                'routes.required' => 'Select Page for this Manager',
                'type.required' => 'Admin Role Type is required'
            ]);


            \DB::transaction( function() use($request) {

                $admin = Admin::create([
                    'name' => $request->name,
                    'mobile' => $request->mobile,
                    'email' => $request->email,
                    'token' => strtoupper(str_random(25)),
                    'password' => \Hash::make($request->password),
                    'type' => $request->type,
                ]);

                collect($request->routes)->map( function($route) use ($admin) {
                    AdminRoute::create(['admin_id' => $admin->id, 'route' => $route]);
                });

            });

            return redirect()->route('admin-manager-view')->with(['success' => 'New Admin Manager has been created']);
        }

        return view('admin.manager.create', ['routes' => AdminRoute::restrictedRoutes()]);
    }

    public function update(Request $request)
    {
        $admin = Admin::find($request->id);

        if($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required',
                'mobile' => 'required|regex:/^[987][0-9]{9}$/|digits:10|unique:admins,mobile,'.$admin->id,
                'email' => 'required|email|unique:admins,email,'.$admin->id,
                'password' => 'min:6|nullable',
                'routes' => 'required',
                'type' => 'required'
            ],[
                'name.required' => 'Admin Manager Name is Required',
                'mobile.required' => 'Mobile Number is Required',
                'mobile.unique' => 'This Mobile Number is already exist',
                'mobile.regex' => 'Invalid Mobile Number',
                'mobile.digits' => 'Mobile Number must be 10 digits',
                'email.required' => 'Email Address is required',
                'email.email' => 'Invalid Email Address',
                'email.unique' => 'This Email Address is already exist',
                'password.min' => 'Password length should be 6 or more',
                'routes.required' => 'Select Pages for this Manager',
                'type.required' => 'Admin Role Type is required'
            ]);


            \DB::transaction( function() use($request, $admin) {

                activity()->causedBy(\Session::get('admin'))->performedOn($admin)
                    ->inLog('AdminUpdate')
                    ->withProperties(collect([
                        'attributes'=> ['name' => $request->name, 'mobile' => $request->mobile, 'email' => $request->email, 'status' => $request->status],
                        'old'=> ['name' => $admin->name, 'mobile' => $admin->mobile, 'email' => $admin->email, 'status' => $admin->status],
                    ]))->log('Admin Update');

                $admin->name = $request->name;
                $admin->mobile = $request->mobile;
                $admin->email = $request->email;
                $admin->status = $request->status;
                $admin->type = $request->type;
                $admin->password = !empty($request->password) ? \Hash::make($request->password) : $admin->password;
                $admin->save();

                AdminRoute::where('admin_id', $admin->id)->delete();

                collect($request->routes)->map( function($route) use ($admin) {
                    AdminRoute::create(['admin_id' => $admin->id, 'route' => $route]);
                });
            });

            return redirect()->route('admin-manager-view')->with(['success' => $admin->name . ' Admin Manager has been updated']);
        }

        // --- Retrieve Selected Routes --- //
        $exist_routes = AdminRoute::where('admin_id', $admin->id)->select(['route'])->get();

        $routes = collect(AdminRoute::restrictedRoutes())->map( function($route) use ($exist_routes) {

            if(count($exist_routes->filter( function($exist_route) use ($route){
                    return $route->key == $exist_route->route;
                })) > 0)
                $route->selected = true;

            return $route;
        });

        return view('admin.manager.update', [
            'admin' => $admin,
            'routes' => $routes
        ]);
    }
}
