<?php

namespace App\Http\Controllers\Admin\Business;

use App\Library\Tree;
use App\Models\Binary\BinaryCalculation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BinaryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->tracking_id)
        {
            if (!$current_user = User::with(['detail'])->whereTrackingId($request->tracking_id)->first())
                return redirect()->back()->with(['error' => 'Invalid Tracking Id']);
        }
        else {
            $current_user = User::with(['detail'])->first();
        }


        $direct_children = $current_user->children->load(['detail']);


        return view('admin.business.binary.tree', [
            'primary' => $current_user,
            'children' => (new Tree())->create($direct_children),
        ]);
    }

    public function status(Request $request)
    {
        if($request->tracking_id) {

            if(!$user = User::with('detail')->whereTrackingId($request->tracking_id)->first())
                return redirect()->back()->with(['error' => 'Invalid Tracking ID']);

            $binary_calculations = BinaryCalculation::where('user_id', $user->id)->orderBy('id', 'desc')->get();

            return view('admin.business.binary.status', [
                'user' => $user, 'binary_calculations' => $binary_calculations
            ]);
        }

        return view('admin.business.binary.status');
    }

    public function searchSuggestion(Request $request)
    {
        if (strlen($request->search) < 3)
            return response()->json([]);

        $results = User::with(['detail:user_id,first_name,last_name'])->search($request->search, [
            'tracking_id', 'detail.first_name', 'detail.last_name'
        ])->take(10)->get()->map(function ($user) {

            return [
                'user_details' => $user->detail->full_name . ' (' . $user->tracking_id . ')',
                'tracking_id' => $user->tracking_id
            ];
        });

        return response()->json($results);

    }
}
