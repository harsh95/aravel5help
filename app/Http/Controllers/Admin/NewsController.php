<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {
        return view('admin.news.index', [
            'items' => News::with('admin')->orderBy('id','asc')->paginate(20)
        ]);
    }

    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'title' => 'required',
                'message' => 'required',
            ],[
                'title.required' => 'Title is Required',
                'message.required' => 'Message is Required',
            ]);

            News::create([
                'admin_id' => \Session::get('admin')->id,
                'title'=> $request->title,
                'message'=> $request->message,
                'status' => 1
            ]);

            return redirect()->route('admin-news')->with(['success' => 'News has been added successfully..']);
        }
        return view('admin.news.create');
    }

    public function update(Request $request)
    {
        if (!$news = News::whereId($request->id)->first())
            return redirect()->back()->with(['error' => 'Invalid Data for Update']);

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'title' => 'required',
                'message' => 'required',
                'status' => 'required'
            ],[
                'title.required' => 'Title is Required',
                'message.required' => 'Message is Required',
                'status.required' => 'Status is Required'
            ]);

            $news->title = $request->title;
            $news->message = $request->message;
            $news->status = $request->status;
            $news->save();

            return redirect()->route('admin-news')->with(['success' => 'News has been updated successfully..']);
        }

        return view('admin.news.update', [
            'news' => $news
        ]);
    }

}
