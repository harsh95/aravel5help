<?php

namespace App\Http\Controllers\Admin;

use App\Models\Package;
use App\Models\Pin;
use App\Models\PinPaymentDetail;
use App\Models\PinRequest;
use App\Models\PinTransferHistory;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PinController extends Controller
{
    public function index(Request $request)
    {
        $pins = Pin::with([
            'package', 'user.detail', 'package_order.user.detail'
        ])->search($request->search, [
            'number', 'amount' ,'user.tracking_id', 'user.username', 'user.mobile'
        ])->filterDate($request->dateRange);

        if ($request->status)
        {
            if ($request->status == 'unused')
                $pins = $pins->whereStatus(Pin::UNUSED);
            elseif ($request->status == 'used')
                $pins = $pins->whereStatus(Pin::USED);
            elseif ($request->status == 'blocked')
                $pins = $pins->whereStatus(Pin::BLOCKED);
        }

        if (!empty($request->package_id))
            $pins = $pins->wherePackageId($request->package_id);

        if ($request->export)
        {
            if (empty($request->status) && empty($request->dateRange) && empty($request->package_id) && empty($request->search))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            \Excel::create('Pin Details', function ($excel) use ($pins) {

                $excel->sheet('Pin Details from ' . env('BRAND_NAME'), function ($sheet) use ($pins) {

                    $data = $pins->get()->map( function ($pin) {

                        return [
                            'Created Date' => $pin->created_at->format('M d, Y'),
                            'User' => $pin->user->detail->full_name,
                            'Tracking Id' => $pin->user->tracking_id,
                            'Package' => $pin->package->name,
                            'Package Amount' => $pin->package->amount,
                            'Pin' => $pin->number,
                            'Used By' => $pin->package_order ? $pin->package_order->detail->full_name . ' (' . $pin->package_order->tracking_id . ')' : '',
                            'Status' => $pin->status_label
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.pin-module.index', [
            'pins' => $pins->orderBy('id', 'desc')->paginate(30),
            'packages' => Package::active()->get()
        ]);
    }

    public function create(Request $request)
    {
        if($request->isMethod('post'))
        {
            $package = Package::whereId($request->package_id)->first();

            $this->validate($request,[
                'pin_qty' => 'required|numeric',
                'package_id' => 'required',
                'user_id' => 'required|exists:users,id',
                'payment_mode' => 'required',
                'bank_name' => 'required|nullable',
                'payment_at' => 'required',
            ],[
                'pin.required' => 'Number of Pins is Required',
                'pin.numeric' => 'Number of Pins is Must be Numeric',
                'package_id.required' => 'Package is Required',
                'user_id.required' => 'User is Required',
                'user_id.exists' => 'Invalid Tracking Id of Searched User',
            ]);

            \DB::transaction( function () use ($request, $package) {

                $payment_detail = PinPaymentDetail::create([
                    'payment_mode' => $request->payment_mode,
                    'qty' => $request->pin_qty,
                    'bank_name' => $request->bank_name,
                    'payment_at' => Carbon::parse($request->payment_at),
                    'remarks' => $request->remarks
                ]);

                for ($i=0; $i < $request->pin_qty; $i++)
                {
                    Pin::create([
                        'admin_id' => \Session::get('admin')->id,
                        'pin_payment_id' => $payment_detail->id,
                        'package_id' => $package->id,
                        'number' => strtoupper(str_random(15)),
                        'amount' => $package->amount,
                        'user_id' => $request->user_id
                    ]);
                }
            });

            return redirect()->route('admin-pin-view')->with(['success' => 'Pins has been Created.']);

        }


        return view('admin.pin-module.generate', [
            'packages' => Package::active()->get(),
            'payment_modes' => json_decode(\File::get(public_path('data/payment_mode.json')))
        ]);
    }

    public function update(Request $request)
    {
        $pin = Pin::with(['user.detail','package'])->whereId($request->id)->first();


        if($request->isMethod('post') && $request->status != 1)
        {
            $pin->status = $request->status;
            $pin->save();

            return redirect()->route('admin-pin-view')->with([
                'success' => 'Pin Status has been Updated'
            ]);
        }

        return view('admin.pin-module.update', ['pin' => $pin]);

    }

    /**
     * !important: Currently Its Based on User table.
     * Changes required in below method if User will Generate the Pins or Multiple Upgrade Available
     */
    public function transfer_history(Request $request)
    {
        $pin = Pin::with(['package', 'user.detail', 'admin'])->whereId($request->id)->first();

        $transfer_histories = PinTransferHistory::wherePinId($request->id)->with(['sender.detail', 'receiver.detail'])->get();

        return view('admin.pin-module.history', [
            'pin' => $pin,
            'transfer_histories' => $transfer_histories
        ]);

    }



    public function requestView(Request $request)
    {
        $pinRequests = PinRequest::with(['package','user.detail'])->search($request->search, [
            'reference_number','user.tracking_id', 'user.mobile', 'user.detail.first_name', 'user.detail.last_name'
        ])->filterDate($request->dateRange);

        if ($request->status)
        {
            if ($request->status == 1)
                $pinRequests = $pinRequests->whereStatus(PinRequest::PENDING);
            elseif ($request->status == 2)
                $pinRequests = $pinRequests->whereStatus(PinRequest::APPROVED);
            elseif ($request->status == 3)
                $pinRequests = $pinRequests->whereStatus(PinRequest::REJECTED);
        }

        if($request->package)
        {
            $pinRequests = $pinRequests->where('package_id', $request->package);
        }

        $packages = Package::active()->get();

        return view('admin.pin-module.request.index', [
            'pin_requests' => $pinRequests->orderBy('id','desc')->paginate(30),
            'packages' => $packages
        ]);
    }


    public function requestUpdate(Request $request)
    {
        $pin_request = PinRequest::with(['package','user.detail'])->whereId($request->id)->first();

        if($request->isMethod('post'))
        {
            $pin_request->status = $request->status;
            $pin_request->remarks = $request->remarks;
            $pin_request->save();

            return redirect()->route('admin-pin-request-view')->with(['success' => 'Pin Request has been Updated..']);
        }

        return view('admin.pin-module.request.update', [
            'pin_request' => $pin_request
        ]);
    }


}
