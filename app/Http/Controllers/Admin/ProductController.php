<?php

namespace App\Http\Controllers\Admin;

use App\Library\Helper;
use App\Library\ImageUpload;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductPrice;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::with(['admin', 'category', 'prices'])->search($request->search, [
            'name'
        ])->filterDate($request->dateRange);

        return view('admin.product.index',[
            'products' => $products->orderBy('id', 'desc')->paginate(30)
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'category_id' => 'required',
                'name' => 'required|unique:products,name',
                'code' => 'required|unique:product_prices,code' ,
                'images' => 'required',
                'description' => 'min:5',
                'price' => 'numeric|required',
                'distributor_price' => 'numeric|required',
                'points' => 'required',
                'gst_rate' => 'required',
                'hsn_code' => 'required'
            ],[
                'name.required' => 'Product name is required field',
                'name.unique' => 'Product name is already taken',
                'category_id.required' => 'Category is required',
                'code.required' => 'Product code is required field',
                'code.unique' => 'Product code must be unique',
                'description.min' => 'Minimum 5 character are compulsory',
                'distributor_price.required' => 'Distributor Price is Required for Products',
                'points.required' => 'Points is Required for Products',
            ]);

            $image_uploader = (new ImageUpload());

            $image_names = collect($request->file('images'))->map(function ($request_image) use ($image_uploader) {

                $image_name = null;

                if(!$request_image && !$request_image->isValid())
                    return false;

                if($image_uploader->process($request_image,ImageUpload::DOCUMENT_TYPE))
                    $image_name = $image_uploader->store(env('PRODUCT_IMAGE_PATH'), 'LOCAL');

                return $image_name;

            })->filter(function ($image_name) {
                return $image_name;
            })->values();


            \DB::transaction(function () use ($request, $image_names) {

                $product = Product::create([
                    'admin_id' => \Session::get('admin')->id,
                    'category_id' => $request->category_id,
                    'name' => $request->name,
                    'slug' => str_slug($request->name, '-') . '-' . $request->code,
                    'description' => $request->description,
                ]);

                ProductPrice::create([
                    'product_id' => $product->id,
                    'code' => $request->code,
                    'images' => count($image_names) > 0 ? json_encode($image_names) : null,
                    'price' => $request->price,
                    'distributor_price' => $request->distributor_price,
                    'points' => $request->points,
                    'qty' => 0,
                    'gst' => json_encode([
                        'percentage' => $request->gst_rate, 'code' => $request->hsn_code
                    ])
                ]);

            });

            return redirect()->route('admin-product-view')->with(['success' => 'New Products has been Added']);
        }

        return view('admin.product.create',[
            'categories' => Category::selectRaw('id as value, name as label')->whereNull('parent_id')->active()->get()->toArray(),
        ]);
    }

    public function update(Request $request)
    {

        if ($request->deleteImage) {

            $product_price = ProductPrice::whereId($request->product_price_id)->first();

            $images = collect($product_price->images)->reject(function ($image) use ($request) {
                return $image == $request->deleteImage;
            })->values()->toArray();

            \Storage::disk('spaces')->delete(env('PRODUCT_IMAGE_PATH') . $request->deleteImage);

            $product_price->images = count($images) > 0 ? json_encode($images) : null;
            $product_price->save();

            return redirect()->route('admin-product-update', ['id' => $product_price->product_id])->with(['success' => 'Image has been deleted']);

        }

        $product = Product::whereId($request->id)->first();

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'category_id' => 'required',
                'name' => 'required|unique:products,name,' . $product->id,
                'description' => 'min:5',
                'prices.*.code' => 'required' ,
                'prices.*.price' => 'numeric|required',
                'prices.*.distributor_price' => 'numeric|required',
                'prices.*.points' => 'required',
                'prices.*.gst_rate' => 'required',
                'prices.*.hsn_code' => 'required'
            ],[
                'name.required' => 'Product name is required field',
                'name.unique' => 'Product name is already taken',
                'category_id.required' => 'Category is required',
                'prices.*.code.required' => 'Product code is required field',
                'description.min' => 'Minimum 5 character are compulsory',
                'prices.*.distributor_price.required' => 'Distributor Price is Required for Products',
                'prices.*.price.required' => 'Distributor Price is Required for Products',
                'prices.*.points.required' => 'Points is Required for Products',
                'prices.*.gst_rate.required' => 'Gst Rate is required',
                'prices.*.hsn_code.required' => 'HSN Code is required',
            ]);

            $product_code_exists = collect(Helper::arrayToObject($request->prices))->map(function ($product_price) {
                return ProductPrice::where('id', '<>', $product_price->id)->whereCode($product_price->code)->first();
            })->filter(function ($product_price) {
                return $product_price;
            });

            if (count($product_code_exists) > 0) {
                return redirect()->back()->with(['error' => $product_code_exists->first()->code . ' Product Code is Already Exists, try different one']);
            }


            $product_prices = collect($request->prices)->map(function ($product_price) {

                $image_uploader = (new ImageUpload());

                if (!isset($product_price['images'])) {
                    return $product_price;
                }

                $image_names = collect($product_price['images'])->map(function ($request_image) use ($image_uploader) {

                    $image_name = null;

                    if(!$request_image && !$request_image->isValid())
                        return false;

                    if($image_uploader->process($request_image,ImageUpload::DOCUMENT_TYPE))
                        $image_name = $image_uploader->store(env('PRODUCT_IMAGE_PATH'), 'LOCAL');

                    return $image_name;

                })->filter(function ($image_name) {
                    return $image_name;
                })->values();

                $product_price['image_names'] = $image_names;

                return $product_price;

            });


            $product->category_id = $request->category_id;
            $product->name = $request->name;
            $product->slug = str_slug($request->name, '-') . '-' . $request->code;
            $product->description = $request->description;
            $product->status = $request->status;
            $product->save();

            collect(Helper::arrayToObject($product_prices))->map(function ($request_product_price) {

                $product_price = ProductPrice::whereId($request_product_price->id)->first();

                $images = null;
                if (isset($request_product_price->image_names) && count($request_product_price->image_names) > 0) {

                    /* Convert Image Objects to Array */
                    $request_images = collect($request_product_price->image_names)->toArray();

                    if (count($product_price->images) > 0)
                        $images = array_merge($request_images, $product_price->images);
                    else
                        $images = $request_product_price->image_names;
                }

                ProductPrice::whereId($request_product_price->id)->update([
                    'code' => $request_product_price->code,
                    'images' => json_encode($images ? $images : $product_price->images),
                    'price' => $request_product_price->price,
                    'distributor_price' => $request_product_price->distributor_price,
                    'points' => $request_product_price->points,
                    'qty' => 0,
                    'gst' => json_encode([
                        'percentage' => $request_product_price->gst_rate, 'code' => $request_product_price->hsn_code
                    ])
                ]);

            });

            return redirect()->route('admin-product-view')->with(['success' => 'Item has been updated successfully']);

        }

        return view('admin.product.update', [
            'product' => $product,
            'categories' => Category::selectRaw('id as value, name as label')->whereNull('parent_id')->active()->get()->toArray(),
        ]);
    }


}
