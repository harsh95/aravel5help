<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Models\SpecialIncome;
use App\Models\User;
use App\Models\UserStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SpecialIncomeController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->removeIncomeRequest)
        {
            $user_status = SpecialIncome::whereId($request->removeIncomeRequest)->first();
            $user_status->status = SpecialIncome::INACTIVE;
            $user_status->save();

            activity()->causedBy(\Session::get('admin'))->performedOn($user_status)
                ->inLog('Special Income Stopped')
                ->withProperties(collect([
                    'attributes'=> ['special_income' => SpecialIncome::INACTIVE],
                    'old' => ['special_income' => SpecialIncome::ACTIVE]
                ]))->log('Special Income Stopped');

            return redirect()->route('admin-user-special-income-view')->with(['success' => 'User`s Special Income generation has been stopped']);
        }

        if ($request->addIncomeRequest)
        {
            if (!$user = User::whereId($request->user_id)->exists())
                return redirect()->back()->with(['error' => 'Invalid Tracking Id']);

            if($sincome =SpecialIncome::whereUserId($request->user_id)->first())
            {
                $sincome->status = SpecialIncome::ACTIVE;
                $sincome->save();
                return redirect()->route('admin-user-special-income-view')
                    ->with(['success' => 'User has been added in Special Income List']);
            }

            $status = SpecialIncome::create(['user_id' => $request->user_id , 'admin_id' => \Session::get('admin')->id , 'status' => SpecialIncome::ACTIVE
                ]);

            activity()->causedBy(\Session::get('admin'))->performedOn($status)
                ->inLog('Payment Continue')
                ->withProperties(collect([
                    'attributes'=> ['income' => SpecialIncome::ACTIVE],
                    'old' => ['income' => SpecialIncome::INACTIVE]
                ]))->log('User Payment Continue');

            return redirect()->route('admin-user-special-income-view')
                ->with(['success' => 'User has been added in Special Income List']);
        }

        $users = SpecialIncome::whereStatus(SpecialIncome::ACTIVE)->with('user','user.detail')->search($request->search, [
            'user.tracking_id', 'user.detail.first_name'
        ])->orderBy('id', 'desc')->paginate(30);

        return view('admin.special-income.index', [
            'users' => $users
        ]);
    }

    public function percentage(Request $request)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'percentage' => 'numeric|min:1|max:100',
            ]);
            $setting = Setting::whereName($request->name)->first();

            $test['percentage'] = $request->percentage;

            $setting->value = json_encode($test);
            $setting->save();
            return back()->with(['success' => 'Percentage updated successfully']);
        }

        if (!$setting = Setting::whereName($request->name)->first())
            return redirect()->back()->with(['error' => 'Invalid Setting Module']);

        return view('admin.special-income.percentage',[
            'setting' => $setting
        ]);
    }
}
