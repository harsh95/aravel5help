<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Models\Order;
use App\Models\ProductPrice;
use App\Models\StoreManager\Offer;
use App\Models\StoreManager\OfferDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OfferController extends Controller
{
    public function index(Request $request)
    {
        $offers = Offer::search($request->search, [
            'name'
        ])->filterDate($request->dateRange)->orderBy('status', 'asc')->paginate(30);

        return view('admin.store-manager.offer.index',[
            'offers' => $offers
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'type' => 'required',
                'min_billing_amount' => 'numeric|required_if:type,' . Offer::BILLING_TO_PRODUCT,
                'max_billing_amount' => 'numeric|required_if:type,' . Offer::BILLING_TO_PRODUCT,
                'condition_items.*.product_price_id' => 'required_if:type,' . Offer::PRODUCT_TO_PRODUCT,
                'condition_items.*.qty' => 'required_if:type,' . Offer::PRODUCT_TO_PRODUCT,
                'offer_items.*.product_price_id' => 'required',
                'offer_items.*.qty' => 'required|numeric'
            ],[
                'name.required' => 'Offer Name is required',
                'type.required' => 'Offer type is required',
                'min_billing_amount.required_if' => 'Min Billing Amount is required',
                'max_billing_amount.required_if' => 'Min Billing Amount is required',
                'condition_items.*.product_price_id.required_if' => 'Condition Item is Required',
                'offer_items.*.product_price_id.required' => 'Offer Item is Required'
            ]);

            if(Carbon::parse($request->start_date)->gt(Carbon::parse($request->end_date)))
                return redirect()->back()->with(['error' => 'Start date should be greater than End Date']);

            \DB::transaction(function () use ($request) {

                $offer = Offer::create([
                    'admin_id' => \Session::get('admin')->id,
                    'name' => $request->name,
                    'min_amount' => $request->type != Offer::PRODUCT_TO_PRODUCT ? $request->min_billing_amount : 0,
                    'max_amount' => $request->type != Offer::PRODUCT_TO_PRODUCT ? $request->max_billing_amount : 0,
                    'type' => $request->type,
                    'start_at' => Carbon::parse($request->start_date),
                    'end_at' => Carbon::parse($request->end_date),
                    'status' => Offer::ACTIVE
                ]);

                OfferDetail::create([
                    'offer_id' => $offer->id,
                    'condition_items' => $request->type == Offer::PRODUCT_TO_PRODUCT ? collect($request->condition_items)->toJson() : null,
                    'offer_items' => collect($request->offer_items)->toJson(),

                ]);

            });

            return redirect()->route('admin-store-manager-offer-view')->with(['success' => 'New Offer has been created']);
        }

        $items = ProductPrice::with(['product:id,name'])->latest()->active()->get();

        return view('admin.store-manager.offer.create', [
            'items' => $items
        ]);

    }

    public function update(Request $request)
    {
        if (!$offer = Offer::whereId($request->id)->with('details')->first())
            return redirect()->back()->withInput()->with(['error' => 'Invalid Offer Details, Try again']);

        if ($request->isMethod('post'))
        {
            $this->validate($request, [
                'name' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'min_billing_amount' => 'numeric|required_if:type,' . Offer::BILLING_TO_PRODUCT,
                'max_billing_amount' => 'numeric|required_if:type,' . Offer::BILLING_TO_PRODUCT,
                'condition_items.*.product_price_id' => 'required_if:type,' . Offer::PRODUCT_TO_PRODUCT,
                'condition_items.*.qty' => 'required_if:type,' . Offer::PRODUCT_TO_PRODUCT,
                'offer_items.*.product_price_id' => 'required',
                'offer_items.*.qty' => 'required|numeric'
            ],[
                'name.required' => 'Offer Name is required',
                'min_billing_amount.required_if' => 'Min Billing Amount is required',
                'max_billing_amount.required_if' => 'Min Billing Amount is required',
                'condition_items.*.product_price_id.required_if' => 'Condition Item is Required',
                'offer_items.*.product_price_id.required' => 'Offer Item is Required'
            ]);

            if(Carbon::parse($request->start_date)->gt(Carbon::parse($request->end_date)))
                return redirect()->back()->with(['error' => 'Start date should be greater than Date']);

            \DB::transaction(function () use ($request, $offer) {

                $offer->name = $request->name;
                $offer->min_amount = $offer->type != Offer::PRODUCT_TO_PRODUCT ? $request->min_billing_amount : 0;
                $offer->max_amount = $offer->type != Offer::PRODUCT_TO_PRODUCT ? $request->max_billing_amount : 0;
                $offer->start_at = Carbon::parse($request->start_date);
                $offer->end_at = Carbon::parse($request->end_date);
                $offer->status = $request->status;
                $offer->save();

                if ($offer->type == Offer::PRODUCT_TO_PRODUCT)
                    $offer->details->condition_items = collect($request->condition_items)->toJson();

                $offer->details->offer_items = collect($request->offer_items)->toJson();
                $offer->details->save();

            });

            return redirect()->route('admin-store-manager-offer-view')->with(['success' => 'Offer has been updated']);
        }

        $items = ProductPrice::with(['product:id,name'])->latest()->active()->get()->map(function ($item) use ($offer) {
            $offer_items = collect($offer->details->offer_items)->pluck('product_price_id')->toArray();
            $item->selected = in_array($item->id, $offer_items) ? true : false;
            $item->label = $item->product->name . '('. $item->code . ')';
            $item->value = $item->id;
            return $item;
        });

        return view('admin.store-manager.offer.update', [
            'offer' => $offer,
            'items' => $items
        ]);
    }

    public function report(Request $request)
    {
        if (!$offer = Offer::whereId($request->id)->first())
            return redirect()->back()->withInput()->with(['error' => 'Invalid Offer Details, Try again']);

        $orders = Order::whereOfferId($offer->id)->with(['user.detail:user_id,first_name,last_name,title'])->search($request->search, [
            'customer_order_id', 'tracking_id'
        ])->filterDate($request->dateRange)->latest()->paginate(30);

        return view('admin.store-manager.offer.report', [
            'offer' => $offer,
            'orders' => $orders
        ]);
    }
}
