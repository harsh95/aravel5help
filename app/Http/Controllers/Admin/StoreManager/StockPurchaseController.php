<?php

namespace App\Http\Controllers\Admin\StoreManager;

use App\Library\Helper;
use App\Models\ProductPrice;
use App\Models\StoreManager\MinimumStock;
use App\Models\StoreManager\StockPurchase;
use App\Models\StoreManager\StockPurchaseDetail;
use App\Models\StoreManager\StockTransaction;
use App\Models\StoreManager\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockPurchaseController extends Controller
{
    public function index(Request $request)
    {
        $records = StockPurchase::with(['supplier', 'admin'])->search($request->search, [
            'supplier.name'
        ])->filterDate($request->dateRange)->latest()->paginate(30);

        return view('admin.store-manager.stock-purchase.index', [
            'records' => $records
        ]);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {

            $validator = \Validator::make($request->all(), [
                'supplier_id' => 'required|exists:suppliers,id',
                'delivery_type' => 'required',
            ], [
                'supplier_id.required' => 'Select Supplier to create Stock Purchase',
                'supplier_id.exists' => 'Invalid Supplier',
            ]);

            if ($validator->fails())
                return response()->json(['status' => false, 'message' => $validator->errors()->first()]);

            $form_request = Helper::arrayToObject($request->all());

            $order_items = collect($form_request->order_items)->filter(function ($order_item) {
                return $order_item->selected_qty > 0;
            });

            if (count($order_items) == 0)
                return response()->json(['status' => false, 'message' => 'Select at least one Item to Create Stock Purchase']);

            \DB::transaction(function () use ($form_request, $order_items) {

                $stock_purchase = StockPurchase::create([
                    'admin_id' => \Session::get('admin')->id,
                    'supplier_id' => $form_request->supplier_id,
                    'delivery_type' => $form_request->delivery_type,
                    'amount' => $form_request->order_details->amount,
                    'tax_amount' => $form_request->order_details->gst,
                    'total' => $form_request->order_details->total_amount,
                    'remarks' => $form_request->remarks
                ]);

                collect($order_items)->map(function ($order_item) use ($stock_purchase) {

                    StockPurchaseDetail::create([
                        'stock_purchase_id' => $stock_purchase->id,
                        'product_price_id' => $order_item->id,
                        'qty' => $order_item->selected_qty,
                        'amount' => $order_item->purchase_price,
                        'gst' => json_encode([
                            'percentage' => $order_item->tax_percentage,
                            'code' => $order_item->tax_code,
                        ])
                    ]);

                    StockTransaction::create([
                        'admin_id' => \Session::get('admin')->id,
                        'stock_purchase_id' => $stock_purchase->id,
                        'product_price_id' => $order_item->id,
                        'type' => StockTransaction::CREDIT,
                        'qty' => $order_item->selected_qty
                    ]);

                });

                return $stock_purchase;

            });

            $request->session()->flash('success', 'New Stock Purchase Generated');

            return response()->json(['status' => true, 'message' => 'New Stock Purchase Generated', 'route' => route('admin-store-manager-stock-purchase-view')]);

        }

        $items = ProductPrice::with(['product:id,name'])->latest()->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'code' => $item->code,
                'name' => $item->product->name,
                'selected_qty' => 0,
                'purchase_price' => 0,
                'tax_percentage' => $item->gst->percentage,
                'tax_code' => $item->gst->code,
            ];
        });

        return view('admin.store-manager.stock-purchase.create', [
            'items' => $items,
            'suppliers' => Supplier::selectRaw('id as value, name as label, mobile, city')->active()->get(),
        ]);
    }

    public function detail(Request $request)
    {
        if (!$stock_purchase = StockPurchase::whereId($request->id)->with(['details.product_price.product', 'supplier'])->first())
            return redirect()->back()->with(['error' => 'Invalid Stock Purchase Records, Try Again']);

        return view('admin.store-manager.stock-purchase.detail', [
            'stock_purchase' => $stock_purchase
        ]);
    }

    public function stockList(Request $request)
    {
        $difference_query = 'SUM(COALESCE(CASE WHEN stock_transactions.type = 1 THEN stock_transactions.qty END,0)) - SUM(COALESCE(CASE WHEN stock_transactions.type = 2 THEN stock_transactions.qty END,0))';

        $records = StockTransaction::selectRaw('product_price_id, ' . $difference_query .' as balance')->with(['product_price.product:id,name','minimum_stock' => function($q){
            $q->whereNotNull('admin_id');
        }])->search($request->search, [
            'product_price.code', 'product_price.product.name'
        ])->orderBy('balance', 'desc')->groupBy('product_price_id');


        $out_of_stock = StockTransaction::selectRaw('stock_transactions.product_price_id, ' . $difference_query .' as balance')
            ->havingRaw($difference_query . ' = 0')->groupBy('product_price_id')->havingRaw('balance = 0')->get();

        $near_to_empty = StockTransaction::selectRaw('stock_transactions.product_price_id, ' . $difference_query .' as balance, minimum_stocks.qty as required_qty')->joinRelations('minimum_stock')
            ->whereNotNull('minimum_stocks.admin_id')->groupBy('stock_transactions.product_price_id')->havingRaw('required_qty > balance AND balance > 0')->get();

        if($request->download == 'yes') {

            \Excel::create('Company Stock', function ($excel) use ($records, $request) {

                $excel->sheet('Sheet1', function ($sheet) use ($records, $request) {

                    if (!empty($request->dateRange) || !empty($request->search))
                        $records = $records->orderBy('created_at', 'desc')->get();
                    else
                        $records = $records->orderBy('id', 'desc')->paginate(50);

                    $data = $records->map(function ($record) {

                        return [
                            'Product' => $record->product_price->product->name,
                            'Product Code' => $record->product_price->code,
                            'Stock' => number_format($record->balance),
                            'Minimum Qty' => $record->minimum_stock ? $record->minimum_stock->qty : 0,
                            'Distributor Price' => number_format($record->product_price->distributor_price),

                        ];
                    })->toArray();
                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.store-manager.stock-purchase.stock-list', [
            'records' => $records->paginate(30),
            'out_of_stock' => count($out_of_stock),
            'near_to_empty' => count($near_to_empty),
        ]);
    }

    public function updateMinimumQty(Request $request)
    {
        if(!MinimumStock::where('product_price_id', $request->product_price_id)->whereNotNull('admin_id')->exists()){

            if($request->qty < 1)
                return redirect()->back()->with(['error' => 'Invalid Quantity, Try Again']);

            MinimumStock::create([
                'admin_id' => \Session::get('admin')->id,
                'product_price_id' =>  $request->product_price_id,
                'qty' => $request->qty
            ]);

            return redirect()->route('admin-store-manager-stock-list')->with(['success' => 'Product Minimum Quantity Updated Successfully']);

        }
        else{

            if($request->qty < 1)
                return redirect()->back()->with(['error' => 'Invalid Quantity, Try Again']);

            $product_price = MinimumStock::where('product_price_id',$request->product_price_id)->whereNotNull('admin_id')->first();

            $product_price->admin_id = \Session::get('admin')->id;
            $product_price->qty = $request->qty;
            $product_price->save();

            return redirect()->route('admin-store-manager-stock-list')->with(['success' => 'Product Minimum Quantity Updated Successfully']);
        }

    }
}
