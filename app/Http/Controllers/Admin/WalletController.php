<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WalletController extends Controller
{
    public function index(Request $request)
    {
        $wallets = Wallet::with(['user.detail'])->search($request->search, [
            'user.username', 'user.tracking_id', 'user.detail.first_name', 'user.mobile'
        ])->filterDate($request->dateRange);

        if ($request->payout_id)
            $wallets = $wallets->wherePayoutId($request->payout_id)->whereType(Wallet::CREDIT_TYPE);

        if ($request->incomeType)
            $wallets = $wallets->whereIncomeType($request->incomeType);

        if ($request->export)
        {
            if ( empty($request->dateRange) && empty($request->search))
                return redirect()->back()->with(['error' => 'At least one filter is required to export the report']);

            \Excel::create('Wallet Details', function ($excel) use ($wallets) {

                $excel->sheet('Wallet Details from ' . env('BRAND_NAME'), function ($sheet) use ($wallets) {

                    $data = $wallets->get()->map( function ($wallet) {

                        return [
                            'Created Date' => $wallet->created_at->format('M d, Y'),
                            'User' => $wallet->user->detail->full_name ." (".$wallet->user->tracking_id.")" ,
                            'Total' => $wallet->total,
                            'TDS' => $wallet->tds,
                            'Admin' => $wallet->admin_charge,
                            'Amount' => $wallet->amount,
                            'Income Types' => $wallet->income_type_name,
                            'Remarks' => $wallet->remarks
                        ];

                    })->toArray();

                    $sheet->fromArray($data);
                });

            })->download('xls');
        }

        return view('admin.wallet.index', [
            'wallets' => $wallets->orderBy('id', 'desc')->paginate(50)
        ]);
    }

    public function status(Request $request)
    {
        if($request->tracking_id)
        {
            if(!$user = User::with('detail')->whereTrackingId($request->tracking_id)->first())
                return redirect()->route('admin-wallet-status')->with(['error' => 'Invalid Tracking ID']);
        }
        else {
            $user = User::with('detail')->first();
        }

        $wallets = Wallet::with(['user'])->whereUserId($user->id)->paginate(20);

        return view('admin.wallet.status', [
            'user' => $user,
            'wallets' => $wallets
        ]);
    }

    public function creditDebit(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'user_id' => 'required|exists:users,id',
                'type' => 'required',
                'amount' => 'required|numeric',
                'remarks' => 'required',
            ],[
                'user_id.required' => 'Transfer To is Required',
                'user_id.exists' => 'Invalid Tracking Id',
                'type.required' => 'Type is Required',
                'amount.required' => 'Amount is Required',
                'amount.numeric' => 'Amount is Must be Numeric',
                'remarks.required' => 'Remarks is Required'
            ]);


            Wallet::create([
                'admin_id' => \Session::get('admin')->id,
                'user_id' => $request->user_id,
                'type' => $request->type,
                'income_type' => Wallet::OTHER_TYPE,
                'total' => $request->amount,
                'amount' => $request->amount,
                'remarks' => $request->remarks,
            ]);

            return redirect()->route('admin-wallet-report')->with(['success' => 'Wallet has been Updated..']);
        }
        return view('admin.wallet.transaction');

    }

    public function balanceReport(Request $request)
    {
        $difference_query = 'SUM(COALESCE(CASE WHEN type = 1 THEN amount END,0)) - SUM(COALESCE(CASE WHEN type = 2 THEN amount END,0))';

        $reports = Wallet::selectRaw('user_id, ' . $difference_query .' as balance')->with(['user.detail'])->search($request->search, [
           'user.tracking_id', 'user.username', 'user.mobile'
        ])->orderBy('balance', 'desc')->groupBy('user_id')->paginate(30);

        return view('admin.wallet.balance-report', [
            'reports' => $reports
        ]);
    }
}
