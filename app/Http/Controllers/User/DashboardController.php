<?php

namespace App\Http\Controllers\User;

use App\Models\MonthlyBv;
use App\Models\NestedSetUser;
use App\Models\OrderBvLog;
use App\Models\Payout;
use App\Models\Pin;
use App\Models\Support;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index()
    {
        $user = User::whereId(\Session::get('user')->id)->first();


        $current_month = now()->format("M Y");
        $previous_month = now()->subMonthNoOverflow()->format("M Y");

        $team_records = MonthlyBv::whereIn('month_name', [$current_month, $previous_month])->whereUserId($user->id)->get();

        $self_records = OrderBvLog::whereUserId($user->id)
            ->selectRaw("COALESCE(SUM(bv), 0) as total_bv, DATE_FORMAT(created_at, '%b %Y') as month_name")
            ->whereNull('leg')->whereRaw("DATE_FORMAT(created_at, '%b %Y') in ('{$current_month}', '{$previous_month}')")->groupBy('month_name')->get();

        $current_month_self_bv = optional($self_records->where('month_name', $current_month)->first())->total_bv;
        $previous_month_self_bv = optional($self_records->where('month_name', $previous_month)->first())->total_bv;

        $current_month_team_record = $team_records->where('month_name', $current_month)->first();
        $previous_month_team_record = $team_records->where('month_name', $previous_month)->first();

        $total_self_bv = OrderBvLog::whereNull('leg')->whereUserId($user->id)->sum('bv');

        $total_team_record = MonthlyBv::whereUserId($user->id)->get();

        return view('user.dashboard', [
            'user' => $user,
            'user_status' => $user->status,
            'total_payout' => Payout::whereUserId($user->id)->sum('total'),
            'sponsors' => User::whereSponsorBy($user->id)->count(),
            'children' => NestedSetUser::childrenCount($user->id),
            'open_supports' => Support::whereNull('parent_id')->whereUserId($user->id)->whereStatus(Support::OPEN)->count(),
            'unused_pins' => Pin::whereStatus(Pin::UNUSED)->whereUserId(\Session::get('user')->id)->count(),
            'binary_total' => $user->current_binary_status,
            'active_sponsors' => User::whereSponsorBy($user->id)->whereNotNull('paid_at')->count(),
            'current_month_self_bv' => $current_month_self_bv,
            'previous_month_self_bv' => $previous_month_self_bv,
            'current_month_team_record' => $current_month_team_record,
            'previous_month_team_record' => $previous_month_team_record,
            'total_self_bv' => $total_self_bv,
            'total_team_record' => $total_team_record
        ]);
    }
}
