<?php

namespace App\Http\Controllers\User;

use App\Library\ImageUpload;
use App\Models\Country;
use App\Models\NestedSetUser;
use App\Models\Setting;
use App\Models\State;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserBank;
use App\Models\UserDetail;
use App\Models\UserStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Webpatser\Uuid\Uuid;

class ProfileController extends Controller
{
    
    public function index(Request $request)
    {
        $user = User::with(['address'])->whereId(\Session::get('user')->id)->first();

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'city' => 'required',
                'profile_image' => 'image|nullable',
            ], [
                'city.required' => 'City is Required',
                'profile_image.image' => 'Profile Image is invalid'
            ]);

            $user_address =  UserAddress::whereUserId($user->id)->first();
            $user_detail = UserDetail::whereUserId($user->id)->first();

            if($request->hasFile('profile_image') && $request->file('profile_image')->isValid())
            {
                $image = (new ImageUpload());

                if(!$image->process($request->file('profile_image'),ImageUpload::PROFILE_IMAGE_TYPE))
                    return redirect()->back()->with(['error' => 'Invalid / Broken or Corrupted Image, Try Another']);

                if($user_detail->image)
                    $image->delete(env('USER_PROFILE_IMAGE_PATH'), $user_detail->image, 'LOCAL');

                $user_detail->image = $image->store(env('USER_PROFILE_IMAGE_PATH'), 'LOCAL');
            }

            $user_address->address = $request->address;
            $user_address->landmark = $request->landmark;
            $user_address->city = $request->city;
            $user_address->district = $request->district;
            $user_address->state_id = $request->state_id;
            $user_address->country_id = $request->country_id;
            $user_address->pincode = $request->pincode;
            $user_address->save();


            $user_detail->birth_date = Carbon::parse($request->birth_date)->format('Y-m-d');

            $user_detail->nominee_name = $request->nominee_name;
            $user_detail->nominee_relation = $request->nominee_relation;
            $user_detail->nominee_birth_date = Carbon::parse($request->nominee_birth_date)->format('Y-m-d');
            $user_detail->save();

            return redirect()->route('user-account-profile')->with(['success' => 'Your Profile has been Updated!!..']);
        }

        $nominees = \File::get(public_path('data/nominees.json'));

        return view('user.profile.index',[
            'nominee_relation' => json_decode($nominees),
            'states' => State::active()->get(),
            'country' => Country::whereIn('id',[99,148])->active()->get(),
            'user' => $user
        ]);

    }

    public function identityCard(Request $request)
    {
        $user = User::whereId(\Session::get('user')->id)->first();

        return view('user.profile.identity-card', [
            'user' => $user
        ]);
    }


    public function password(Request $request)
    {
        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'old_password' => 'required|min:6|exists:users,password,id,'.\Session::get('user')->id,
                'new_password' => 'required|min:6'
            ], [
                'old_password.required' => 'Old Password is required',
                'old_password.min' => 'Old Password length should be 6 or more',
                'old_password.exists' => 'Invalid Old Password',
                'new_password.required' => 'New Password is required',
                'new_password.min' => 'New Password length should be 6 or more'
            ]);

            $user = User::find(\Session::get('user')->id);
            $user->password = $request->new_password;
            $user->save();

            return redirect()->back()->with(['success' => 'User Password has been Updated.']);
        }
        return view('user.profile.change-password');
    }


    public function walletPassword(Request $request)
    {

        if($request->isMethod('post'))
        {
            $this->validate($request,[
                'old_wallet_password' => 'required|min:6|exists:users,wallet_password,id,'.\Session::get('user')->id,
                'new_wallet_password' => 'required|min:6',
            ], [
                'old_wallet_password.required' => 'Old Wallet Password is required',
                'old_wallet_password.min' => 'Old Wallet Password length should be 6 or more',
                'old_wallet_password.exists' => 'Invalid Old Wallet Password',
                'new_wallet_password.required' => 'New Wallet Password is required',
                'new_wallet_password.min' => 'New Wallet Password length should be 6 or more',
            ]);

            $user = User::find(\Session::get('user')->id);
            $user->wallet_password = $request->new_wallet_password;
            $user->save();

            return redirect()->back()->with(['success' => 'User Wallet Password has been Updated.']);
        }
        return view('user.profile.change-wallet-password');
    }

    public function welcomeLetter()
    {
        $user = User::with(['detail', 'package'])->whereId(\Session::get('user')->id)->first();

        return view('user.profile.welcome-letter', [
            'user' => $user
        ]);
    }

    public function packageInvoice()
    {
        $user = User::with(['detail', 'package'])->whereId(\Session::get('user')->id)->first();

        if ($user->pin_id == null)
            return redirect()->back()->with(['error' => 'Invoice is not Available, Kindly Upgrade your account to get Invoice']);

        $setting = Setting::where('name','Company Profile')->first();

        return view('user.profile.invoice', [
            'user' => $user,
            'setting' => $setting
        ]);
    }

    public function packageReceipt()
    {
        $user = User::with(['detail', 'package.items'])->whereId(\Session::get('user')->id)->first();

        if ($user->pin_id == null)
            return redirect()->back()->with(['error' => 'Receipt is not Available, Kindly Upgrade your account to get Receipt']);

        $setting = Setting::where('name','Company Profile')->first();

        return view('user.profile.receipt', [
            'user' => $user,
            'setting' => $setting
        ]);
    }


    public function getChildren(Request $request)
    {
        if (!isset($request->tracking_id))
            return response()->json(['status' => false, 'message' => 'Tracking ID is required']);

        $nested_current_user = NestedSetUser::whereUserId(\Session::get('user')->id)->first();

        $searched_user = User::whereTrackingId($request->tracking_id)->orWhere('username', $request->tracking_id)->first();

        if (!$nested_search_user = NestedSetUser::whereUserId($searched_user->id)->first())
            return response()->json(['status' => false, 'message' => 'We can not retrieve the search user data, try after some time for reporting']);

        if (!$nested_search_user->isDescendantOf($nested_current_user))
            return response()->json(['status' => false, 'message' => 'User is not available or User is not downline of ' . \Session::get('user')->tracking_id]);

        return response()->json([
            'status' => true,
            'user' => [
                'id' => $searched_user->id,
                'name' => $searched_user->detail->full_name,
                'tracking_id' => $searched_user->tracking_id
            ]
        ]);

    }



}
