<?php

namespace App\Http\Controllers\User;

use App\Models\Reward;
use App\Models\RewardAchiever;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RewardController extends Controller
{
    public function index()
    {

        $reward_achievers = RewardAchiever::with(['reward'])->whereUserId(\Session::get('user')->id)->get();

        $reward = Reward::get()->map(function ($reward) use ($reward_achievers) {

            $achiever = collect($reward_achievers)->whereStrict('reward_id', $reward->id)->first();
            $reward->achiever_id = $achiever ? $achiever->id : null;
            $reward->achiever_date = $achiever ? $achiever->created_at : null;
            $reward->achiever_status = $achiever ? '1' : null;
            return $reward;

        });

        return view('user.reward.index',[
            'rewards' => $reward,
            'reward_achievers' => $reward_achievers
        ]);
    }

}
