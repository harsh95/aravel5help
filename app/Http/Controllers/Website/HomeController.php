<?php

namespace App\Http\Controllers\Website;

use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function video()
    {
        return view('website.video');
    }

    public function index()
    {
        $banners = Banner::whereStatus(Banner::ACTIVE)->orderBy('id','asc')->get();

        return view('website.home',[
            'banners' => $banners
        ]);
    }
}
