<?php

namespace App\Http\Middleware\Store;

use Closure;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!\Session::has('store'))
            return redirect()->route('store-login')->with(['error' => 'Store Session has been Expired, Login Again']);

        return $next($request);
    }
}
