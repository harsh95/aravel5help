<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $mobile
 * @property string $token
 * @property string $last_logged_in_ip
 * @property string $last_logged_in_at
 * @property int $status 1: Active, 2: Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereLastLoggedInAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereLastLoggedInIp($value)
 * @method static \Sofa\Eloquence\Builder|\App\Models\Admin newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Admin newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Admin query()
 * @property int $type 1: Master, 2: Manager, 3: Employee
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereType($value)
 */
class Admin extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    const MASTER = 1, MANAGER = 2, EMPLOYEE = 3;

    protected $hidden = [
        'password', 'token'
    ];

    protected $fillable = [
        'name', 'password', 'email', 'mobile', 'status', 'type', 'token', 'last_logged_in_ip', 'last_logged_in_at'
    ];
}
