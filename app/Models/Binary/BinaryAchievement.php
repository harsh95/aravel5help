<?php

namespace App\Models\Binary;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Binary\BinaryAchievement
 *
 * @property int $id
 * @property int $user_id
 * @property int $calculation_id
 * @property int $calculation_reference_id
 * @property int $status 1: Active, 2: Inactive
 * @property int|null $wallet_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereCalculationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereCalculationReferenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryAchievement whereWalletId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryAchievement newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryAchievement newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryAchievement query()
 */
class BinaryAchievement extends Model
{
    use Eloquence;

    const ACTIVE =  1, INACTIVE = 2;

    protected $fillable = [
        'user_id', 'calculation_id', 'calculation_reference_id', 'reference_users', 'wallet_id', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
