<?php

namespace App\Models\Binary;

use App\Models\PackageOrder;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Binary\BinaryCalculationPvLog
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $package_order_ids
 * @property string|null $leg
 * @property float $pv
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculationPvLog newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculationPvLog newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Binary\BinaryCalculationPvLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog whereLeg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog wherePackageOrderIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog wherePv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Binary\BinaryCalculationPvLog whereUserId($value)
 * @mixin \Eloquent
 */
class BinaryCalculationPvLog extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'package_order_ids', 'leg', 'pv', 'amount', 'created_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getPackageOrderIdsAttribute($value)
    {
        return $value ? json_decode($value, true) : [];
    }

    public function createLog(PackageOrder $package_order, $user_id, $leg)
    {
        $pv = $package_order->pv;

        if($log = self::whereUserId($user_id)->whereLeg($leg)->whereDate('created_at', '=', $package_order->created_at->toDateString())->first()) {

            if (!in_array($package_order->id, $log->package_order_ids))
            {
                $package_order_ids = array_merge($log->package_order_ids, [$package_order->id]);

                self::whereId($log->id)->update([
                    'pv' => \DB::raw("pv + $pv"),
                    'amount' => \DB::raw("amount + $package_order->amount"),
                    'package_order_ids' => json_encode($package_order_ids)
                ]);
            }

        }
        else {

            self::create([
                'user_id' => $user_id, 'leg' => $leg, 'pv' => $pv, 'amount' => $package_order->amount, 'package_order_ids' => json_encode([$package_order->id]), 'created_at' => $package_order->created_at
            ]);
        }
    }
}
