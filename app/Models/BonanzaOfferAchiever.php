<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonanzaOfferAchiever extends Model
{
    protected $fillable = ['offer_id', 'user_id', 'wallet_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bonanza_offer()
    {
        return $this->belongsTo(BonanzaOffer::class);
    }
}
