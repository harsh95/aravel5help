<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Event
 *
 * @property int $id
 * @property int $category_id
 * @property int|null $admin_id
 * @property string $name
 * @property string $location
 * @property string $state_id
 * @property string|null $description
 * @property int $status 1: Active, 2: Inactive
 * @property string $start_at
 * @property string $end_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\EventCategory $category
 * @property-read \App\Models\State $state
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Event newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Event newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event running()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereStateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Event whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'category_id', 'admin_id', 'name', 'location', 'state_id', 'description', 'status', 'start_at', 'end_at'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function category()
    {
        return $this->belongsTo(EventCategory::class, 'category_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }

    public function scopeRunning($q)
    {
        $q->where('start_at', '<=', now()->toDateTimeString())->where('end_at', '>=', now()->toDateTimeString());
    }
}
