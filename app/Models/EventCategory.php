<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\EventCategory
 *
 * @property int $id
 * @property string $name
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory active()
 * @method static \Sofa\Eloquence\Builder|\App\Models\EventCategory newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\EventCategory newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\EventCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\EventCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class EventCategory extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }
}
