<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\MonthlyBv
 *
 * @property int $id
 * @property int $user_id
 * @property float $lft
 * @property float $rgt
 * @property string $month_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\MonthlyBv newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\MonthlyBv newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\MonthlyBv query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MonthlyBv whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MonthlyBv whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MonthlyBv whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MonthlyBv whereMonthName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MonthlyBv whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MonthlyBv whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MonthlyBv whereUserId($value)
 * @mixin \Eloquent
 */
class MonthlyBv extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'lft', 'rgt', 'month_name',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
