<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\News
 *
 * @property int $id
 * @property int|null $admin_id
 * @property string $title
 * @property string $message
 * @property int $status 1:Active, 2:Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\News whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\News newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\News newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\News query()
 */
class News extends Model
{
    use Eloquence;

    protected $fillable = [
        'admin_id','title','message','status'
    ];

    const Active = 1, Inactive = 2;

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }


}
