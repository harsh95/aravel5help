<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PackageItem
 *
 * @property int $id
 * @property int $package_id
 * @property string|null $gst
 * @property string $name
 * @property float $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $actual_amount
 * @property-read mixed $tax_amount
 * @property-read \App\Models\Package $package
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageItem newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageItem newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PackageItem extends Model
{
    use Eloquence;

    protected $fillable = [
        'package_id', 'name', 'amount', 'gst'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function getGstAttribute($value)
    {
        return $value ? json_decode($value) : null;
    }

    public function getTaxAmountAttribute()
    {
        return round(($this->amount*$this->gst->percentage)/((float)100+(float)$this->gst->percentage), 4);
    }

    public function getActualAmountAttribute()
    {
        return round(($this->amount - $this->tax_amount), 2);
    }

    public static function getGstPercentage()
    {
        return [0, 5, 12, 18, 28];
    }
}
