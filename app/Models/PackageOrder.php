<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\PackageOrder
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $admin_id
 * @property int|null $placed_by
 * @property int $package_id
 * @property int $pin_id
 * @property float $amount
 * @property int $pv_calculation 0: No, 1: Yes, 2: Not Available
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\Package $package
 * @property-read \App\Models\Pin $pin
 * @property-read \App\Models\User|null $placedBy
 * @property-read \App\Models\User $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageOrder newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageOrder newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\PackageOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePackageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePinId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePlacedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePvCalculation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder whereUserId($value)
 * @mixin \Eloquent
 * @property float $pv
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PackageOrder wherePv($value)
 */
class PackageOrder extends Model
{
    use Eloquence;
    const NO = 0, YES = 1, NOT_AVAILABLE = 2;

    protected $fillable = [
        'user_id', 'admin_id', 'placed_by', 'package_id', 'pin_id', 'amount', 'pv', 'pv_calculation'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function placedBy()
    {
        return $this->belongsTo(User::class, 'placed_by');
    }

    public function package()
    {
        return $this->belongsTo(Package::class);
    }

    public function pin()
    {
        return $this->belongsTo(Pin::class);
    }
}
