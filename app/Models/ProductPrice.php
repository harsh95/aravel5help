<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\ProductPrice
 *
 * @property int $id
 * @property int $product_id
 * @property string $code
 * @property string|null $images
 * @property float $price
 * @property float $distributor_price
 * @property float $points
 * @property int $qty
 * @property string $gst Json: percentage & hsn/sac code
 * @property int $status 1: Active, 2: Inactive
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $primary_image
 * @property-read \App\Models\Product $product
 * @method static \Sofa\Eloquence\Builder|\App\Models\ProductPrice newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ProductPrice newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\ProductPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereDistributorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice wherePoints($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ProductPrice active()
 */
class ProductPrice extends Model
{
    use Eloquence;

    protected $fillable = [
        'product_id', 'code', 'images', 'price', 'distributor_price', 'points', 'qty', 'gst', 'status'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getGstAttribute($value)
    {
        return $value ? json_decode($value) : null;
    }

    public function getImagesAttribute($value)
    {
        return $value ? json_decode($value, true) : [];
    }

    public function scopeActive($q)
    {
        $q->where('status', 1);
    }

    public function getPrimaryImageAttribute()
    {
        $images = $this->images;

        if (count($images) == 0)
            return null;

        if (!$images)
            return null;

        if (!isset($images[0]))
            return null;

        return env('PRODUCT_IMAGE_URL') . $images[0];
    }

    public static function getGstPercentage()
    {
        return [0, 3, 5, 12, 18, 28];
    }
}
