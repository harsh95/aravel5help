<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\Reward
 *
 * @property int $id
 * @property string $name
 * @property int $pair
 * @property string $reward
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Sofa\Eloquence\Builder|\App\Models\Reward newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Reward newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\Reward query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward wherePair($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereReward($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Reward whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Reward extends Model
{
    use Eloquence;

    protected $fillable = [
        'name' ,'pair', 'reward'
    ];


}
