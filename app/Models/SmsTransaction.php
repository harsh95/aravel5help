<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\SmsTransaction
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereUserId($value)
 * @mixin \Eloquent
 * @property-read \App\Models\User|null $user
 * @method static \Sofa\Eloquence\Builder|\App\Models\SmsTransaction newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SmsTransaction newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SmsTransaction query()
 * @property string|null $category
 * @property string|null $mobile
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SmsTransaction whereMobile($value)
 */
class SmsTransaction extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'category', 'mobile', 'message'
    ];

    public function user()
    {
       return $this->belongsTo(User::class);
    }
}
