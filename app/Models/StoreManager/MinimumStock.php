<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use App\Models\ProductPrice;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\MinimumStock
 *
 * @property int $id
 * @property int $product_price_id
 * @property int|null $admin_id
 * @property int|null $store_id
 * @property int $qty
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \App\Models\ProductPrice $product_price
 * @property-read \App\Models\StoreManager\Store|null $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\MinimumStock newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\MinimumStock newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\MinimumStock query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\MinimumStock whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\MinimumStock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\MinimumStock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\MinimumStock whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\MinimumStock whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\MinimumStock whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\MinimumStock whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MinimumStock extends Model
{
    use Eloquence;
    protected $fillable = [
        'product_price_id', 'admin_id', 'store_id', 'qty'
    ];

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}
