<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StoreStockRequest
 *
 * @property int $id
 * @property int|null $admin_id
 * @property int $store_id
 * @property float $amount
 * @property int $status 1: Pending, 2: Approved, 3: Rejected
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin|null $admin
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\StoreManager\StoreStockRequestDetail[] $details
 * @property-read int|null $details_count
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockRequest newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockRequest newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockRequest query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $discount
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereDiscount($value)
 * @property-read \App\Models\StoreManager\Store $receiver_store
 * @property int|null $receiver_store_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequest whereReceiverStoreId($value)
 */
class StoreStockRequest extends Model
{
    use Eloquence;

    CONST PENDING = 1, APPROVED = 2, REJECTED = 3;

    protected $fillable = [
        'admin_id', 'store_id', 'receiver_store_id', 'amount', 'discount', 'status', 'remarks'
    ];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public function receiver_store()
    {
        return $this->belongsTo(Store::class, 'receiver_store_id');
    }

    public function details()
    {
        return $this->hasMany(StoreStockRequestDetail::class, 'request_id');
    }
}
