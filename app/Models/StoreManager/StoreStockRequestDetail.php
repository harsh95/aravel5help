<?php

namespace App\Models\StoreManager;

use App\Models\ProductPrice;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StoreStockRequestDetail
 *
 * @property int $id
 * @property int $request_id
 * @property int $product_price_id
 * @property float $price
 * @property float $distributor_price
 * @property int $qty
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $total_amount
 * @property-read \App\Models\ProductPrice $product_price
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockRequestDetail newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockRequestDetail newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreStockRequestDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereDistributorPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereProductPriceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereQty($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereRequestId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $selling_price
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreStockRequestDetail whereSellingPrice($value)
 */
class StoreStockRequestDetail extends Model
{
    use Eloquence;

    protected $fillable = [
        'request_id', 'product_price_id', 'price', 'distributor_price', 'qty', 'selling_price'
    ];

    public function product_price()
    {
        return $this->belongsTo(ProductPrice::class);
    }

    public function getTotalAmountAttribute()
    {
        return $this->qty * $this->distributor_price;
    }
}
