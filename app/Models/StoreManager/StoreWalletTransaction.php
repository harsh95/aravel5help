<?php

namespace App\Models\StoreManager;

use App\Models\Admin;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreManager\StoreWalletTransaction
 *
 * @property int $id
 * @property int|null $admin_id
 * @property int $store_id
 * @property float $opening_amount
 * @property float $amount
 * @property float $closing_amount
 * @property int $type 1: Credit, 2: Debit
 * @property string|null $remarks
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin $admin
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreWalletTransaction newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreWalletTransaction newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreManager\StoreWalletTransaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereAdminId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereClosingAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereOpeningAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereRemarks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $supply_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereSupplyId($value)
 * @property int|null $status 1: Active, 2: Inactive
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreManager\StoreWalletTransaction whereStatus($value)
 */
class StoreWalletTransaction extends Model
{
    use Eloquence;

    const CREDIT_TYPE = 1, DEBIT_TYPE = 2;
    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'admin_id', 'store_id', 'opening_amount', 'amount', 'closing_amount', 'type', 'remarks', 'status'
    ];

    public function admin()
    {
        return $this->hasOne(Admin::class, 'id', 'admin_id');
    }

    public function store()
    {
        return $this->hasOne(Store::class, 'id', 'store_id');
    }
}
