<?php

namespace App\Models;

use App\Models\StoreManager\Store;
use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\StoreStatus
 *
 * @property int $aadhar_card 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $address_proof 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property int $gst 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $id
 * @property int $pancard 1: Pending, 2: Verified, 3:InProgress, 4:Rejected
 * @property int $store_id
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\StoreManager\Store $store
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreStatus newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreStatus newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\StoreStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus whereAadharCard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus whereAddressProof($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus whereGst($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus wherePancard($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus whereStoreId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StoreStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StoreStatus extends Model
{
    use Eloquence;

    const KYC_PENDING = 1, KYC_VERIFIED = 2, KYC_INPROGRESS = 3, KYC_REJECTED = 4;

    protected $fillable = [
        'store_id',
        'pancard',
        'aadhar_card',
        'bank_account',
        'gst',
        'address_proof',
    ];

    public function store()
    {
        return $this->belongsTo(Store::class);
    }

    public static function  getKycStatuses()
    {
        return [
            self::KYC_PENDING => 'PENDING',
            self::KYC_VERIFIED => 'VERIFIED',
            self::KYC_INPROGRESS => 'IN PROGRESS',
            self::KYC_REJECTED => 'REJECTED',
        ];
    }
}
