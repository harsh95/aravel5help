<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\SupportCategory
 *
 * @property int $id
 * @property string $name
 * @property int $status 1: Active, 2: Inactive
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SupportCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\SupportCategory newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SupportCategory newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\SupportCategory query()
 */
class SupportCategory extends Model
{
    use Eloquence;

    const ACTIVE = 1, INACTIVE = 2;

    protected $fillable = [
        'name', 'status'
    ];

    public function scopeActive($query)
    {
        return $query->where('status', self::ACTIVE);
    }
}
