<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

/**
 * App\Models\UserBank
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $bank_name
 * @property string|null $account_name
 * @property string|null $account_number
 * @property string|null $branch
 * @property string|null $ifsc
 * @property int $type 1: Saving, 2: Current
 * @property string|null $city
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereAccountName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereAccountNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereBranch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereIfsc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereUserId($value)
 * @mixin \Eloquent
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserBank newModelQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserBank newQuery()
 * @method static \Sofa\Eloquence\Builder|\App\Models\UserBank query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserBank whereCity($value)
 */
class UserBank extends Model
{
    use Eloquence;

    protected $fillable = [
        'user_id', 'bank_name', 'account_name', 'account_number', 'branch', 'ifsc', 'type', 'city'
    ];
}
