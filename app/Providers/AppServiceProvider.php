<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::directive('refreshBtn', function ($expression) {

            if ($expression)
                return '<a href="/' . \Route::getCurrentRoute()->uri() . '" class="btn btn-icon btn-primary" title="Refresh"> <i class="la la-refresh"></i> </a>';
            else
                return '<a href="/' . \Route::getCurrentRoute()->uri() . '" class="btn btn-theme btn-sm" title="Refresh"> <i class="fa fa-refresh"></i> </a>';

        });

        \Blade::directive('breadcrumb', function ($expression) {

            $breadcrumbs = explode(',',str_replace(['(',')'], '', $expression));

            $breadcrumbs = collect($breadcrumbs)->map(function ($breadcrumb) {

                $single = explode(':', $breadcrumb);

                if ($single[1] != 'active')
                    return '<li class="breadcrumb-item"><a href="' . route($single[1]) . '">' . $single[0] . '</a></li>';
                else
                    return '<li class="breadcrumb-item active">' . $single[0] . '</li>';
            })->toArray();


            return '<div class="" data-pages="parallax">
                        <div class="container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
                            <div class="inner"><ol class="breadcrumb"> ' . implode("", $breadcrumbs) . ' </ol></div>
                        </div>
                    </div>';

        });

        \Blade::if('user', function () {
            return \Session::has('user');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
