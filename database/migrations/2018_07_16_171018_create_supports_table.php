<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable();
            $table->integer('category_id')->unsigned();
            $table->integer('admin_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->text('message');
            $table->string('image')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1: Open, 2: Closed');
            $table->tinyInteger('type')->nullable()->comment('1: Admin to User, 2: User to Admin');
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('support_categories');
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('supports');
    }
}
