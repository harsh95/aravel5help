<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title',10)->comment('Mr, Miss, Mrs, Dr., Sri');
            $table->string('first_name',100);
            $table->string('last_name',100);
            $table->string('image',255)->nullable();
            $table->tinyInteger('apply_pan')->default('2')->nullable()->comment('1:Yes, 2: No');
            $table->string('pan_no',50)->nullable();
            $table->string('nagrikta_card_no',255)->nullable();
            $table->tinyInteger('gender')->comment('1: Male, 2: Female');
            $table->date('birth_date');
            $table->string('nominee_name',100)->nullable();
            $table->string('nominee_relation',50)->nullable();
            $table->date('nominee_birth_date')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_details');
    }
}
