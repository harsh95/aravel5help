<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreStockTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_stock_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->nullable()->unsigned();
            $table->integer('store_id')->index()->unsigned();
            $table->integer('product_price_id')->index()->unsigned();
            $table->integer('order_id')->nullable();
            $table->integer('supply_id')->nullable()->index();
            $table->integer('opening')->default(0);
            $table->integer('qty')->default(0);
            $table->integer('closing')->default(0);
            $table->tinyInteger('type')->comment('1: Credit, 2: Debit');
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_stock_transactions');
    }
}
