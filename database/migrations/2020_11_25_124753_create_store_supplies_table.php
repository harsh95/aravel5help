<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_supplies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->index()->nullable();
            $table->integer('sender_store_id')->unsigned()->index()->nullable();
            $table->integer('admin_supply_id')->nullable();
            $table->integer('store_supply_id')->nullable();
            $table->integer('store_id')->unsigned()->index();
            $table->float('amount', 12, 2);
            $table->float('tax_amount', 12, 2);
            $table->float('total', 12, 2);
            $table->tinyInteger('delivery_type')->default(1)->comment('1: Self Pickup, 2: Courier, 3: Transport');
            $table->string('courier_name')->nullable();
            $table->string('courier_docket_number')->nullable();
            $table->text('remarks')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1: Pending, 2: Delivered');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_supplies');
    }
}
