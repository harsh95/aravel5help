<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreStockRequestDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_stock_request_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('request_id')->index()->unsigned();
            $table->integer('product_price_id')->index()->unsigned();
            $table->float('price',10,2);
            $table->float('distributor_price',10,2);
            $table->float('selling_price',10,2);
            $table->integer('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_stock_request_details');
    }
}
