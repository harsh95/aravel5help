@extends('admin.template.layout')

@section('title', 'Binary Status')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Binary Status:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row m-b-20">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="get">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text primary"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search Tracking Id" name="tracking_id" class="form-control" value="{{ Request::get('tracking_id') }}">
                            </div>
                            <p class="m-t-10 text-center">
                                Search Tracking ID & Get the Binary Status of Particular user With Current & Carry Forward PV or Joining
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if(Request::get('tracking_id'))
            <div class="card">
                <div class="card-body">
                    <h3 class="text-center">{{ $user->detail->full_name }} ({{ $user->tracking_id }})</h3>
                    <div class="table-responsive m-t-10">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th rowspan="2">Close Date</th>
                                <th rowspan="2">Status</th>
                                <th colspan="9" class="text-center">Members</th>
                            </tr>
                            <tr>
                                <th colspan="2" class="text-center bg-warning-dark text-white">Total Associates</th>
                                <th colspan="2" class="text-center bg-danger-light text-white">Total PV</th>
                                <th colspan="2" class="text-center bg-complete-darker text-white">Forward PV</th>
                                <th colspan="2" class="text-center bg-success text-white">New PV</th>
                                <th class="text-center bg-theme-dark text-white">Completed Ratio</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Left</th>
                                <th>Right</th>
                                <th>Left</th>
                                <th>Right</th>
                                <th>Left</th>
                                <th>Right</th>
                                <th>Left</th>
                                <th>Right</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $difference_left = 0; $difference_right = 0; @endphp
                            @foreach($binary_calculations as $index => $record)
                                <tr>
                                    <td>{{ $record->closed_at ? Carbon\Carbon::parse($record->closed_at)->format('d-m-Y') : 'Current Status' }}</td>
                                    <td>
                                        @if($record->status == 0)
                                            <span class="label label-success"> Current </span>
                                        @elseif($record->status == 1)
                                            <span class="label label-primary"> Closed </span>
                                        @elseif($record->status == 2)
                                            <span class="label label-danger"> Rejected </span>
                                        @endif
                                    </td>
                                    <td class="bg-warning-lighter">{{ $record->left }}</td>
                                    <td class="bg-warning-lighter">{{ $record->right }}</td>
                                    <td class="bg-danger-lighter">{{ $record->total_left }}</td>
                                    <td class="bg-danger-lighter">{{ $record->total_right }}</td>
                                    <td class="bg-complete-lighter">{{ $record->forward_left }}</td>
                                    <td class="bg-complete-lighter">{{ $record->forward_right }}</td>
                                    <td class="bg-success-lighter">{{ $record->current_left }}</td>
                                    <td class="bg-success-lighter">{{ $record->current_right }}</td>
                                    <td class="text-center bg-theme-lighter">
                                        {{ ($record->forward_left+$record->current_left)-$difference_left }} :
                                        {{ ($record->forward_right+$record->current_right)-$difference_right }}
                                    </td>
                                    @php $difference_left = $record->forward_left; $difference_right = $record->forward_right; @endphp
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop