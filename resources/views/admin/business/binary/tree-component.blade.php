@if($user)
    <a href="javascript:void(0)" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-html="true" title="" data-content="@include('admin.business.binary.tree-component-content', ['user' => $user])">
        <img src="{{ $user->treeStatus() }}" width="50">
    </a>
    <h6 style="font-size: 0.8rem;">{{ $user->detail->full_name }} </h6>
    <h6 style="font-size: 0.8rem;">
        @if(isset($isPrimary))
            <a href="javascript:void(0)">{{ $user->tracking_id }}</a>
        @else
            <a href="{{ route('admin-business-binary-tree', ['tracking_id' => $user->tracking_id]) }}"> {{ $user->tracking_id }}</a>
        @endif
    </h6>
@else
    <a href="javascript:void(0)">
        <img src="/user-assets/images/tree/empty.svg" width="50">

        @if($level == 1)
            <div class="line down" style="height: 3.5rem;"></div>
        @elseif($level == 2)
            <div class="line down" style="height: 3.5rem;"></div>
        @endif
    </a>
@endif