@extends('admin.template.layout')

@section('title', 'Document List')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Document List:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="input-group">
                                <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="">Select</option>
                                    <option value="1">Pending</option>
                                    <option value="2">Approved</option>
                                    <option value="3">Rejected</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            <a href="{{ route('admin-document-view', ['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status'), 'export' => 'yes']) }}" class="btn btn-info text-white btn-sm"> <i class="fa fa-download"></i> Download </a>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="13%">Date</th>
                            <th>User</th>
                            <th>Type</th>
                            <th>Reference Number</th>
                            <th>Image</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($documents as $index => $document)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($documents, $index) }}</td>
                                <td>{{ $document->created_at->format('M d, Y h:i A') }} </td>
                                <td>
                                    {{ $document->user->detail->full_name }} <br>
                                    ({{ $document->user->tracking_id }})
                                </td>
                                <td>{{ $document->document_name }}</td>
                                <td>{{ $document->number }}</td>
                                <td>
                                    <a href="javascript:void(0)"  onclick="viewImage('{{ env('DOCUMENT_IMAGE_URL').$document->image }}')" class="btn btn-primary btn-xs">
                                        View Image
                                    </a>
                                    @if($document->secondary_image)
                                        <a href="javascript:void(0)"  onclick="viewImage('{{ env('DOCUMENT_IMAGE_URL').$document->secondary_image }}')" class="btn btn-primary btn-xs">
                                            Back Image
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if ($document->status == 1 )
                                        <span class="badge badge-warning">Pending</span>
                                    @elseif ($document->status == 2)
                                        <span class="badge badge-success">Verified</span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-document-update', ['id' => $document->id]) }}" class="btn btn-danger btn-xs m-b-5">Update</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $documents->appends(['search' => Request::get('search'), 'status' => Request::get('status'), 'dateRange' => Request::get('dateRange') ])->links() }}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="imageViewer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="modal_image text-center"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        function viewImage(image_url) {

            INGENIOUS.blockUI(true);
            $('.modal_image').html('<img src="'+image_url+'" id="documentImage" data-zoom-image="'+image_url+'" width="100%" />');

            setTimeout(() => {
                INGENIOUS.blockUI(false);
                $('#imageViewer').modal();
                $('#documentImage').ezPlus({zoomWindowWidth: 500, zoomWindowHeight: 500});
            }, 650);

        }
    </script>
@stop


@section('import-javascript')
    <script type="text/javascript" src="/plugins/select2/js/select2.full.min.js"></script>
    <script src="/plugins/jquery-elevatezoom/jquery.elevatezoom.js"></script>
@stop

@section('page-css')
    <style>
        .zoomContainer{
            z-index: 9999 !important;
        }
    </style>
@stop