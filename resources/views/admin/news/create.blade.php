@extends('admin.template.layout')

@section('title', 'Create News')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,News:admin-news,Create:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="{{ old('title') }}" required autocomplete="off">
                            </div>

                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" name="message" cols="5" rows="5" placeholder="Enter News or Message" >{{ old('remarks') }}</textarea>
                            </div>

                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-primary btn-lg btn-rounded"> Submit </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

