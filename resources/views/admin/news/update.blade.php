@extends('admin.template.layout')

@section('title', 'Update News')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,News:admin-news,Update:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" value="{{ $news->title }}" required autocomplete="off">
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" name="message" cols="5" rows="5">{{ $news->message }}</textarea>
                            </div>
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $news->status == 1 ? 'selected' : null }} >Active</option>
                                    <option value="2" {{ $news->status == 2 ? 'selected' : null }}>Inactive</option>
                                </select>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger btn-lg btn-rounded"> Update </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

