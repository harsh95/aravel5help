@extends('admin.template.layout')

@section('title', 'Create Offer')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Offer:admin-bonanza-offer-view,Create:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" id="bonanza_form" enctype="multipart/form-data"
                              onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Start Date</label>
                                <input type="text" class="form-control date" name="start_at" readonly>
                            </div>
                            <div class="form-group form-group-default">
                                <label>End Date</label>
                                <input type="text" class="form-control date" name="end_at" readonly>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Left</label>
                                        <input type="text" class="form-control" name="lft">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Right</label>
                                        <input type="text" class="form-control" name="rgt">
                                    </div>
                                </div>
                            </div>
                            <select class="form-control mt-3" name="status">
                                <option value="1" {{ old('status') == 1 ? 'selected' : '' }}>Active</option>
                                <option value="2" {{ old('status') == 2 ? 'selected' : '' }}>Inactive</option>
                            </select>
                            <div class="row text-center mt-2">
                                <div class="col-md-12">
                                    <button class="btn btn-success btn-md">
                                        Create
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $('.date').datepicker({
            autoclose: true,
            format: "dd-mm-yyyy",
            startDate: "-99y",
//            endDate: "-18y"
        });

    </script>
@stop