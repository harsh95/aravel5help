@extends('admin.template.layout')

@section('title', 'Offer List')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Offer :active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-3 pull-right">
                            <a href="{{ route('admin-bonanza-offer-create') }}" class="btn btn-success btn-sm pull-right ml-1"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th> Date </th>
                            <th> Name </th>
                            <th> Start At </th>
                            <th> End At </th>
                            <th> Status </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($bonanza_offers as $index => $bonanza_offer)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($bonanza_offers, $index) }}</td>
                                <td>{{ $bonanza_offer->created_at->format('M d, Y') }} </td>
                                <td>{{ $bonanza_offer->name }}</td>
                                <td>{{ \Carbon\Carbon::parse($bonanza_offer->start_at)->toDateString() }}</td>
                                <td>{{ \Carbon\Carbon::parse($bonanza_offer->end_at)->toDateString() }}</td>
                                <td>
                                    @if ($bonanza_offer->status == 1)
                                        <span class="label label-theme"> Active</span>
                                    @else
                                        <span class="label label-danger"> In Active</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-bonanza-offer-update', ['id' => $bonanza_offer->id]) }}" class="btn btn-info btn-xs">
                                        Update
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $bonanza_offers->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop