@extends('admin.template.layout')

@section('title', 'Orders')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Orders:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <select name="order_from" id="" class="form-control">
                                    <option value="" >Order From</option>
                                    <option value="1" {{ old('order_from') == 1 ? 'selected' : '' }}>Store</option>
                                    <option value="2" {{ old('order_from') == 2 ? 'selected' : '' }}>User</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th>Order ID</th>
                            <th>Order From</th>
                            <th>User</th>
                            <th>Amount</th>
                            {{--<th>Shipping Charge</th>--}}
                            <th>Total</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($orders) == 0)
                            <tr>
                                <td class="text-center" colspan="10">
                                    No data found!!!
                                </td>
                            </tr>
                        @endif
                        @foreach($orders as $index => $order)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($orders, $index) }}</td>
                                <td>
                                    <span class="text-dark">{{ $order->created_at->format('M d, Y h:i A') }}</span>
                                </td>
                                <td class="text-danger">{{ $order->customer_order_id }}</td>
                                <td>
                                    @if($order->store_id)
                                        <span class="badge badge-primary">Store</span><br>
                                        {{ $order->store->name }}<br>
                                        ({{ $order->store->tracking_id }})
                                    @else
                                        <span class="badge badge-info">User</span>
                                    @endif
                                </td>
                                <td class="text-primary">
                                    @if($order->user)
                                        {{ $order->user->detail->full_name }} <br>
                                        <b>({{ $order->user->tracking_id }})</b>
                                    @else
                                        {{ $order->customer->name }} <br>
                                        <b>M: {{ $order->customer->mobile }}</b>
                                    @endif
                                </td>
                                <td>{{ number_format($order->amount) }}</td>
                                {{--                                <td>{{ number_format($order->shipping_charge) }}</td>--}}
                                <td>{{ number_format($order->total) }}</td>
                                <td>
                                    @if($order->status == 2)
                                        <span class="label label-warning"> Placed </span>
                                    @elseif($order->status == 3)
                                        <span class="label label-success">Approved </span>
                                    @elseif($order->status == 5)
                                        <span class="label label-danger"> Rejected </span>
                                    @elseif($order->status == 6)
                                        <span class="label label-danger"> Refund </span>
                                    @else
                                        <span class="label label-theme"> {{ \App\Models\Order::getStatus($order->status) }} </span>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-success" href="{{ route('admin-shopping-order-details', ['customer_order_id' => $order->customer_order_id]) }}">Details</a>
                                    <br>
                                    @if(in_array($order->status, [\App\Models\Order::APPROVED]) && $order->admin_id)
                                        <a class="btn btn-xs btn-danger mt-1" href="{{ route('admin-shopping-order-details', ['customer_order_id' => $order->customer_order_id, 'download' => 'receipt']) }}">Invoice
                                        </a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $orders->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status') ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop