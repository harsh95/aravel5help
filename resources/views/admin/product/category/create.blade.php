@extends('admin.template.layout')

@section('title', 'Create Category')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Categories:admin-category-view,Create:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body">
                        <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Parent Category</label>
                                <select name="parent_category_id" class="full-width" data-init-plugin="select2">
                                    <option value="">Select</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group form-group-default">
                                <label>Category Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button class="btn btn-danger"> Create Category </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop