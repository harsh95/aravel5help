@extends('admin.template.layout')

@section('title', 'My Reward Achievement')

@section('content')
    @breadcrumb(Dashboard:admin-dashboard, All Rewards:active)
 <div class="container">
     <div class="content-body">
         <section>
             <div class="card">
                 <div class="card-content">
                     <div class="card-body">
                         <div class="table-responsive">
                             <table class="table table-hover table-bordered">
                                 <thead>
                                 <tr>
                                     <th>#</th>
                                     <th>Reward Name</th>
                                     <th width="15%">Pair</th>
                                     <th>Reward</th>
                                     <th>Action</th>

                                 </tr>
                                 </thead>
                                 <tbody>
                                 @foreach($rewards as $index => $reward)
                                     <tr>
                                         <td>{{ $index+1 }}</td>
                                         <td class="text-danger">
                                           {{ $reward->name }}
                                         </td>
                                         <td>{{ $reward->pair }}</td>
                                         <td>{{ $reward->reward }}</td>
                                         <th>
                                             <a href="{{ route('admin-user-reward-achievers',['reward_id' => $reward->id]) }}" class="btn btn-primary btn-sm"> View Achievers</a>
                                         </th>
                                     </tr>
                                 @endforeach
                                 </tbody>
                             </table>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
     </div>
 </div>
@stop