@extends('admin.template.layout')

@section('title', 'Change Special Income percentage')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Change Special Income percentage:active)

    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-6 offset-md-3 offset-lg-3">
                <div class="card">
                    <div class="card-body" id="walletTransactionCard">
                        <h4>{{ $setting->module_name }}</h4>
                        <form action="" method="post" role="form" id="systemSettingForm">
                            {{ csrf_field() }}
                                <input type="hidden" name="name" value="{{ $setting->name }}">
                                @if($setting->value->percentage)
                                    <div class="form-group form-group-default">
                                        <label>Percentage</label>
                                        <input type="text" class="form-control" name="percentage" value="{{ $setting->value->percentage }}" required autocomplete="off"  onkeypress="INGENIOUS.numericInput(event, false)">
                                    </div>
                                @endif
                            <div class="text-center">
                                <button type="button" class="btn btn-danger systemSettingBtn"> Update </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')

    <script>
        $('.systemSettingBtn').click(function (e) {

            e.preventDefault();

            var form = $('#systemSettingForm');
            var self = $(this);

            self.attr('disabled', true);

            swal({
                title: "Are you sure?",
                text: 'Are you confirm to change this System Setting?',
                icon: "info",
                buttons: true,
                dangerMode: true
            }).then( function (isConfirm) {

                if (isConfirm) {

                    swal('Processing', 'Do not Close or Refresh this page..!!', 'info');
                    form.submit();
                }
                else {
                    self.attr('disabled', false);
                }

            });
        });
    </script>

@stop