@extends('admin.template.layout')

@section('title', 'Stores')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Stores:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group transparent">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                                <input type="text" placeholder="Search" name="search" class="form-control" value="{{ Request::get('search') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="input-group">
                                <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                    <option value="">Status Filter</option>
                                    <option value="1" {{ Request::get('status') ==  '1' ? 'selected' : '' }}>Active</option>
                                    <option value="2" {{ Request::get('status') ==  '2' ? 'selected' : '' }}>Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                        <div class="col-md-1 pull-right">
                            <a href="{{ route('admin-store-manager-store-create') }}" class="btn btn-success btn-sm pull-right"> Create New </a>
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Access</th>
                            <th>Created at</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Type</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($stores) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Stores Available</td>
                            </tr>
                        @endif
                        @foreach($stores as $index => $store)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($stores, $index) }}</td>
                                <td>
                                    <div class="btn-toolbar flex-wrap" role="toolbar">
                                        <div class="btn-group sm-m-t-10">
                                            <a href="{{ route('admin-store-manager-store-account-access', ['id' => $store->id]) }}" target="_blank" class="btn btn-default" data-toggle="tooltip" title="View Account">
                                                <i class="fa fa-universal-access text-primary"></i>
                                            </a>
                                            <a href="{{ route('admin-store-manager-store-wallet-view', ['store_id' => $store->id]) }}" target="_blank" data-toggle="tooltip" class="btn btn-default" title="View Wallet">
                                                <i class="fa fa-google-wallet text-danger"></i>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                                <td>{{ $store->created_at->format('M d, Y h:i A') }}</td>
                                <td class="text-primary">
                                    {{ $store->name }} <br>({{ $store->tracking_id }})<br>
                                    @if( $store->reference_user_id)
                                        <span class="text-danger">
                                         Referencer User: {{ $store->reference_user->username .'('.$store->reference_user->tracking_id .')'}}
                                    </span>
                                    @endif
                                </td>
                                <td>{{ $store->mobile }}</td>
                                <td>
                                    @if($store->type == \App\Models\StoreManager\Store::MASTER_TYPE)
                                        <span class="badge badge-success">MASTER</span>
                                    @elseif($store->type == \App\Models\StoreManager\Store::MINI_TYPE)
                                        <span class="badge badge-info">Mini</span>
                                    @else
                                        <span class="badge badge-warning">Warehouse</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $store->city }}, ({{ $store->state->name }})
                                </td>
                                <td>
                                    @if($store->status == \App\Models\StoreManager\Store::ACTIVE)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-store-manager-store-update', ['id' => $store->id ]) }}" class="btn btn-danger btn-xs"> Update</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $stores->appends(['search' => Request::get('search'), 'dateRange' => Request::get('dateRange'), 'status' => Request::get('status') ])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop