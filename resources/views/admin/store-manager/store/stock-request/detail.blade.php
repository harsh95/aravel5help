@extends('admin.template.layout')

@section('title', 'Store Stock Request Details of Store: ' . $stock_request->store->name)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Store Stock Request:admin-store-manager-store-request-view, Details # {{ $stock_request->store->name }}:active)
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <ul class="list-group">
                            <li class="list-group-item text-primary">
                                <b>Store:</b> <span class="pull-right">{{ $stock_request->store->name }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Store ID:</b> <span class="pull-right">{{ $stock_request->store->tracking_id }}</span>
                            </li>
                            <li class="list-group-item text-danger">
                                <b>Wallet Balance:</b> <span class="pull-right">Rs. {{ number_format($stock_request->store->wallet_balance) }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Contact:</b> <span class="pull-right">{{ $stock_request->store->mobile }}</span>
                            </li>
                            <li class="list-group-item">
                                <b>Location:</b> <span class="pull-right">{{ $stock_request->store->city }}, {{ $stock_request->store->state->name }} - {{ $stock_request->store->pincode }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        @if($stock_request->status != 2)
                            <h6>Action Against Stock Request</h6>
                            <hr>
                            <form action="{{ route('admin-store-manager-store-request-to-supply', ['id' => $stock_request->id]) }}" method="post" id="stockRequestUpdate">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group form-group-default form-group-default-select2">
                                            <label>Status</label>
                                            <select name="status" class="full-width" data-init-plugin="select2" data-disable-search="true" onchange="changeStatus(value)">
                                                <option value="1" {{ $stock_request->status == 1 ? 'selected' : '' }}>PENDING</option>
                                                <option value="2" {{ $stock_request->status == 2 ? 'selected' : '' }}>APPROVED</option>
                                                <option value="3" {{ $stock_request->status == 3 ? 'selected' : '' }}>REJECTED</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="statusApproved d-none">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default form-group-default-select2">
                                                <label>Direct Supply to Store</label>
                                                <select name="direct_supply" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                                    <option value="2">NO</option>
                                                    <option value="1">YES</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default form-group-default-select2">
                                                <label>Direct Supply to Store</label>
                                                <select name="delivery_type" class="full-width" data-init-plugin="select2" data-disable-search="true">
                                                    <option value="">Select</option>
                                                    <option value="1">Self Pickup</option>
                                                    <option value="2">Courier</option>
                                                    <option value="3">Transport</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group form-group-default">
                                                <label>Your Message or Remarks</label>
                                                <textarea class="form-control" name="remarks" cols="30" rows="6" placeholder="Your Message"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-danger" onclick="INGENIOUS.formConfirmation('#stockRequestUpdate', 'Confirm this action for Stock Request')">Submit</button>
                                </div>
                            </form>
                        @else
                            @if($stock_request->status == \App\Models\StoreManager\StoreStockRequest::APPROVED)
                                <div class="alert alert-info">
                                    Request is Approved on {{ $stock_request->updated_at->format('M d, Y h:i A') }}
                                </div>
                            @else
                                <div class="alert alert-danger">
                                    Request is Rejected on {{ $stock_request->updated_at->format('M d, Y h:i A') }}
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Amount</th>
                                <th>BV</th>
                                <th>Qty</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stock_request->details as $index => $detail)
                                <tr>
                                    <td>{{ $index+1 }}</td>
                                    <td>
                                        {{ $detail->product_price->product->name }}
                                        <code class="pull-right">Code: {{ $detail->product_price->code }}</code>
                                    </td>
                                    <td>{{ $detail->distributor_price }}</td>
                                    <td>{{ $detail->product_price->points }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td class="text-right"> {{ number_format($detail->total_amount, 2) }}</td>
                                </tr>
                            @endforeach
                            <tr class="text-white bg-danger bg-darken-1">
                                <td colspan="4" class="text-right">Total</td>
                                <td>{{ collect($stock_request->details)->sum('qty') }}</td>
                                <td class="text-right">{{ number_format(collect($stock_request->details)->sum('total_amount'), 2) }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>

        function changeStatus(status) {

            if (parseInt(status) === 2) {
                $('.statusApproved').removeClass('d-none')
            }
            else {
                $('.statusApproved').addClass('d-none');
            }

        }
    </script>
@stop