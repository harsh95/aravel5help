@extends('admin.template.layout')

@section('title', 'Store Stock Request')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Store Stock Request:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="store_id">
                                    <option value="">Select Store</option>
                                    @foreach($stores as $store)
                                        <option value="{{ $store->id }}" {{ Request::get('store_id') == $store->id ? 'selected' : ''}}>{{ $store->name . ' - ' . $store->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Created At</th>
                            <th width="20%">Store</th>
                            <th>Items</th>
                            <th>Total Amount</th>
                            <th>Status</th>
                            <th>View</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($stock_requests) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Stock Request Available</td>
                            </tr>
                        @endif
                        @foreach ($stock_requests as $index => $stock_request)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($stock_requests, $index) }}</td>
                                <td>{{ $stock_request->created_at->format('M d, Y h:i A') }}</td>
                                <td class="text-primary">{{ $stock_request->store->name }} <br>({{ $stock_request->store->tracking_id }})</td>
                                <td>{{ count($stock_request->details) }} Items</td>
                                <td>{{ number_format($stock_request->amount, 2) }}</td>
                                <td>
                                    @if($stock_request->status == 1)
                                        <span class="badge badge-warning">Pending</span>
                                    @elseif($stock_request->status == 2)
                                        <span class="badge badge-success">Approved</span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-store-manager-store-request-details', ['id' => $stock_request->id ]) }}" class="btn btn-danger btn-sm"> Details</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $stock_requests->appends(['store_id' => Request::get('store_id'), 'dateRange' => Request::get('dateRange')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop