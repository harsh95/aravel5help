@extends('admin.template.layout')

@section('title', 'Update Store ' . $store->tracking_id)

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Stores:admin-store-manager-store-view,Update - {{ $store->tracking_id }}:active)

    <div class="container-fluid container-fixed-lg" id="storeCreatePage">

        <form action="" method="post" role="form" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group form-group-default">
                                <label>Select Store Type</label>
                                <select name="type" class="form-control">
                                    <option value="">---Select---</option>
                                    @foreach(\App\Models\StoreManager\Store::getStoreTypes() as $storeType)
                                        <option value="{{ $storeType->id }}" {{ $store->type == $storeType->id ? 'selected' : null }}>{{ $storeType->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if($store->user_id)
                                <div class="form-group form-group-default">
                                    <label>Store User</label>
                                    <input type="text" class="form-control" value="{{ $store->user->detail->full_name }}" autocomplete="off" disabled>
                                </div>
                            @endif
                            @if($store->reference_user_id)
                                <div class="form-group form-group-default">
                                    <label>Reference User</label>
                                    <input type="text" class="form-control" value="{{ $store->reference_user->detail->full_name }} (TID:{{$store->reference_user->tracking_id}})" autocomplete="off" disabled>
                                </div>
                            @endif
                            <div class="form-group form-group-default">
                                <label>Set New Password</label>
                                <input type="text" class="form-control" name="password" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group form-group-default">
                                        <label>Store Name <span class="text-danger">*</span></label>
                                        <input type="text" name="name" class="form-control" value="{{ $store->name }}" autocomplete="off">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" value="{{ $store->email }}" autocomplete="off">
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Mobile <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" name="mobile" onkeypress="INGENIOUS.numericInput(event)" value="{{ $store->mobile }}" autocomplete="newMobile">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-group-default">
                                        <label>Store / WareHouse Address <span class="text-danger">*</span></label>
                                        <textarea class="form-control" name="address" cols="5" rows="5" placeholder="Enter Store Address" autocomplete="off">{{ $store->address }}</textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default">
                                                <label>City <span class="text-danger">*</span></label>
                                                <input type="text" name="city" class="form-control" value="{{ $store->city }}" autocomplete="off">
                                            </div>
                                            <div class="form-group form-group-default">
                                                <label>Pincode <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" name="pincode" value="{{ $store->pincode }}" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-default form-group-default-select2">
                                                <label>Select State</label>
                                                <select name="state_id" class="full-width" data-init-plugin="select2" required>
                                                    <option value=""> Select State</option>
                                                    @foreach($states as $index => $state)
                                                        <option value="{{ $state->id }}" {{ $state->id == $store->state_id ? 'selected' : null }}>{{ $state->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group form-group-default">
                                                <label>Status</label>
                                                <select name="status" class="form-control">
                                                    <option value="1" {{ $store->status == 1 ? 'selected' : null }}> Active</option>
                                                    <option value="2" {{ $store->status == 2 ? 'selected' : null }}> Inactive</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-danger btn-lg btn-rounded"> Update </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@stop
