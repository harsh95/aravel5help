@extends('admin.template.layout')

@section('title', 'Store Wallet Request')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard,Store Wallet Request:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="get">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                <select  class="full-width" data-init-plugin="select2" name="store_id">
                                    <option value="">Select Store</option>
                                    @foreach($stores as $store)
                                        <option value="{{ $store->id }}" {{ Request::get('store_id') == $store->id ? 'selected' : ''}}>{{ $store->name . ' - ' . $store->city }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-danger btn-sm"> Search </button>
                            @refreshBtn()
                        </div>
                    </div>
                </form>
                <div class="table-responsive m-t-10">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Admin</th>
                            <th width="15%">Store</th>
                            <th width="15%">Amount</th>
                            <th>Details</th>
                            <th>Deposited On</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($wallet_requests) == 0)
                            <tr>
                                <td colspan="10" class="text-center">No Requests found..!!</td>
                            </tr>
                        @endif
                        @foreach($wallet_requests as $index => $wallet_request)
                            <tr>
                                <td>{{ \App\Library\Helper::tableIndex($wallet_requests, $index) }}</td>
                                <td>{{ $wallet_request->created_at->format('M d, Y h:i A') }}</td>
                                <td>{{ $wallet_request->admin_id ? $wallet_request->admin->name : 'N.A' }}</td>
                                <td class="text-primary">{{ $wallet_request->store->name }} <br> ({{ $wallet_request->store->tracking_id }})</td>
                                <td class="font-small-3">{{ number_format($wallet_request->amount, 2) }}</td>
                                <td>
                                    {{ $wallet_request->bank_name }} - {{ $wallet_request->payment_mode }}<br>
                                    <i>{{ $wallet_request->reference_number }}</i>
                                    @if($wallet_request->image)
                                        <br>
                                        <a href="{{ env('PAYMENT_RECEIPT_IMAGE_URL') . $wallet_request->image }}" target="_blank" class="badge badge-info">
                                            Receipt Image
                                        </a>
                                    @endif
                                </td>
                                <td class="text-danger">{{ \Carbon\Carbon::parse($wallet_request->deposited_at)->format('M d, Y h:i A')  }}</td>
                                <td>
                                    @if($wallet_request->status == 1)
                                        <span class="badge badge-warning">Pending</span>
                                    @elseif($wallet_request->status == 2)
                                        <span class="badge badge-success">Approved</span>
                                    @else
                                        <span class="badge badge-danger">Rejected</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('admin-store-manager-wallet-request-update', ['id' => $wallet_request->id]) }}" class="btn btn-theme btn-sm">
                                        Update
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $wallet_requests->appends(['dateRange' => Request::get('dateRange'), 'store_id' => Request::get('store_id')])->links() }}
                </div>
            </div>
        </div>
    </div>
@stop