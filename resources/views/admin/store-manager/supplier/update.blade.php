@extends('admin.template.layout')

@section('title', 'Create Supplier')

@section('content')

    @breadcrumb(Dashboard:admin-dashboard, Supplier:admin-store-manager-supplier-view,Create:active)

    <div class="container-fluid container-fixed-lg">
        <div class="card">
            <div class="card-body">
                <form action="" method="post" role="form" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Name</label>
                                <input type="text" name="name" placeholder="Supplier's Firm or Company Name" class="form-control" value="{{ $supplier->name }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Mobile</label>
                                <input type="number" name="mobile" class="form-control" value="{{ $supplier->mobile }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Other Contact</label>
                                <input type="number" name="other_contact" class="form-control" value="{{ $supplier->other_contact }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control" value="{{ $supplier->email }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default">
                                <label>Gst Number</label>
                                <input type="text" name="gst_number" class="form-control" value="{{ $supplier->gst_number }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group form-group-default form-group-default-select2">
                                <label>Status</label>
                                <select name="status" class="full-width" data-init-plugin="select2">
                                    <option value="1" {{ $supplier->status == 1 ? 'selected' : null }} >Active</option>
                                    <option value="2" {{ $supplier->status == 2 ? 'selected' : null }} >Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <textarea class="form-control" name="address" cols="30" rows="6" placeholder="Supplier Address" data-gramm="true" data-gramm_editor="true" aria-invalid="false" style="z-index: auto; position: relative; line-height: normal; font-size: 14px; transition: none; background: transparent !important;">{{ $supplier->address }}</textarea>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-group-default">
                                        <label>city</label>
                                        <input type="text" name="city" class="form-control" value="{{ $supplier->city }}" required>
                                    </div>
                                    <div class="form-group form-group-default">
                                        <label>Pin Code</label>
                                        <input type="text" name="pincode" class="form-control" value="{{ $supplier->pincode }}" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-default form-group-default-select2">
                                        <label>State</label>
                                        <select name="state_id" class="full-width" data-init-plugin="select2">
                                            @foreach($states as $state)
                                                <option value="{{ $state->id }}" {{ $supplier->state_id == $state->id ? 'selected' : null }}>{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center mt-3">
                        <button class="btn btn-danger"> Update </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop