<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="Tymk Softwares">
    <title>{{ env('BRAND_NAME') }}</title>
    <link rel="apple-touch-icon" href="/user-assets/images/company/favicon.png">
    <link rel="shortcut icon" type="image/x-icon" href="/user-assets/images/company/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN CSS-->
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/fonts/simple-line-icons/style.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/pace.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/bootstrap-extended.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/colors.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/components.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/vertical-menu.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/palette-gradient.min.css">
    <link rel="stylesheet" type="text/css" href="/user-assets/css/style.css?v=1">
    <link rel="stylesheet" href="/user-assets/css/chat.css?v=1">
    <link rel="stylesheet" href="/user-assets/css/priczx-table.css">
    @yield('import-css')
    @yield('page-css')
</head>
<body class="vertical-layout vertical-menu 1-column bg-gradient-directional-grey-blue menu-expanded blank-page blank-page" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="1-column">

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-12 d-flex align-items-center justify-content-center">
                    <div class="col-md-5 col-10 box-shadow-2 p-0">
                        <div class="card border-grey border-lighten-3 px-1 py-1 box-shadow-3 m-0">
                            <div class="card-header border-0">
                                <div class="text-center mb-1">
                                    <img src="/user-assets/images/company/logo.png" alt="{{ env('BRAND_NAME') }}" width="200px">
                                </div>
                                <div class="font-large-1 text-center text-danger">
                                    This Portal is Under Maintenance
                                </div>
                            </div>
                            <div class="card-body text-center pt-0">
                                <img src="/website-assets/images/errors/maintenance.png" width="50%">
                                <p>We're making the system more awesome. <br> We'll be back shortly.</p>
                                <div class="mt-2">
                                    <i class="la la-cog spinner font-large-2 danger"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<script src="/user-assets/js/vendors.min.js" type="text/javascript"></script>
<script src="/user-assets/js/app-menu.js" type="text/javascript"></script>
<script src="/user-assets/js/app.js" type="text/javascript"></script>
</body>
</html>