@extends('store-manager.template.layout')

@section('title', 'POS - Create Order')

@section('content')
    <div class="row" id="posPage">
        <div class="col-lg-3 col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Search <span class="text-danger">{{ config('project.brand') }}</span> Member</h4>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" placeholder="Enter User Id" v-model="tracking_id" name="tracking_id" class="form-control" autocomplete="off">
                            <div class="input-group-prepend">
                                <button class="btn btn-danger btn-sm" type="button" @click="getUser">Search</button>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group" v-if="user">
                        <li class="list-group-item bg-primary text-white">@{{ user.name }} (@{{ user.tracking_id }})</li>
                    </ul>
                    <div class="" v-if="order.amount > 0">
                        <div class="form-group mt-2">
                            <label>Payment Mode</label>
                            <select class="form-control" name="payment_mode" v-model="payment_mode">
                                <option value="" selected>Select Payment Mode</option>
                                @foreach($payment_modes as $payment_mode)
                                    <option value="{{ $payment_mode }}" {{ old('payment_mode') == $payment_mode ? 'selected' : '' }}>
                                        {{ $payment_mode }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Reference Number</label>
                            <input type="text" name="reference_number" placeholder="Enter Reference Number" class="form-control" v-model="reference_number">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card" v-if="order.amount > 0">
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item text-danger font-medium-1">Total BV: <span class="pull-right">@{{ order.total_bv }}</span></li>
                        <li class="list-group-item bg-amber text-dark">Total Qty: <span class="pull-right">@{{ order.total_qty }}</span></li>
                        <li class="list-group-item">Amount: <span class="pull-right">@{{ order.amount }}</span></li>
                        <li class="list-group-item text-white bg-danger bg-darken-1 font-medium-1">Total: <span class="pull-right">@{{ order.total_amount }}</span></li>
                    </ul>
                </div>
            </div>

        </div>
        <div class="col-lg-9 col-md-12">
            <div class="card" v-if="user">
                <div class="card-body">
                    <div class="form-group" v-if="!qualifiedOffer">
                        <label>Search Product / Item</label>
                        <vue-select2 :options="items" place-holder="Search or Select Item" empty-after-select="1" allow-search="0" @change-select2="setProductToOrder"></vue-select2>
                    </div>
                    <hr>
                    <table class="table table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="45%">Item</th>
                            <th width="10%" class="bg-danger text-white">Store Stock</th>
                            <th width="10%">DP Amount</th>
                            <th width="17%">Qty</th>
                            <th width="13%">Total BV</th>
                            <th width="13%">Total</th>
                            <th width="3%"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="(order_item, index) of order_items" :class="order_item.offer_id ? 'text-dark bg-purple bg-lighten-4' : ''">
                            <td>@{{ index+1 }}</td>
                            <td>
                                @{{ order_item.name | str_limit(35) }} <br> <code>Code: @{{ order_item.code }}</code>
                            </td>
                            <td class="bg-danger-lighter">
                                <span class="font-weight-bold text-black">@{{ order_item.balance }}</span>
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.selling_price).toFixed(2) }}</span>
                            </td>
                            <td>
                                <input type="number" min="1" :max="order_item.balance" onkeypress="INGENIOUS.numericInput(event, false)" class="form-control qty-input input-sm" v-model="order_item.selected_qty" v-if="!qualifiedOffer">
                                <span v-if="qualifiedOffer" class="text-bold-700">@{{ order_item.selected_qty }}</span>
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.selected_qty*order_item.bv).toFixed(2) }}</span>
                            </td>
                            <td>
                                <span class="">@{{ parseFloat(order_item.selected_qty*order_item.selling_price).toFixed(2) }}</span>
                            </td>
                            <td>
                                <a href="javascript:void(0)" class="btn btn-social-icon btn-xs btn-danger" v-if="!qualifiedOffer" title="Remove" @click="removeItem(index)">
                                    <span class="la la-close"></span>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="text-center mt-5" v-if="order.amount > 0">
                        <hr>
                        <button class="btn btn-bg-gradient-x-purple-red btn-glow btn-lg" v-if="!qualifiedOffer" type="button" @click="checkOffer">Continue -></button>
                        <button class="btn btn-danger btn-lg" type="button" v-if="qualifiedOffer" @click="createOrder">Place Order</button>
                        <div class="text-center font-medium-2 mt-2" v-if="qualifiedOffer">
                            OR <br>
                            <a href="javascript:void(0)" class="badge badge-danger" @click="removeOffer">Remove Offer</a> to Modify the Order
                        </div>
                    </div>
                    {{-- Item Selection Modal --}}
                    <div class="modal" id="offerItemSelectionModal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Select Any One Option to <b class="text-danger">Apply Offer</b></h4>
                                </div>
                                <div class="modal-body">
                                    <div v-for="(offer_item_option, index) in offer_item_options">
                                        <h5>
                                            Option @{{ (index+1) }}
                                            <button type="button" @click="selectOfferOption(offer_item_option)" class="btn btn-danger btn-sm pull-right">Select Option @{{ (index+1) }}</button>
                                        </h5>
                                        <ul class="list-group mb-1 mt-1">
                                            <li class="list-group-item" v-for="item in offer_item_option">
                                                @{{ item.name | str_limit(40) }}
                                                <span class="pull-right">Qty: @{{ item.selected_qty }}</span>
                                            </li>
                                        </ul>
                                        <hr v-if="index != Object.keys(offer_item_option).length - 1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        Vue.prototype.$http = axios;

        let createOrder = new Vue({
            el: '#posPage',
            data: {
                user: null,
                tracking_id: null,
                qualifiedOffer: null,
                activeOffers: {!! collect($active_offers)->toJson() !!},
                offer_item_options: [],
                items: {!! collect($items)->toJson() !!},
                payment_mode:'',
                reference_number:'',
                order_items: [],
                order: {
                    total_bv: 0, amount: 0, total_qty: 0, wallet: 0, discount: 0, total_amount: 0
                },
            },
            watch: {

                order_items: {
                    handler() {
                        this.calculateSummary();
                    },
                    deep: true
                }
            },
            methods: {
                getUser: function () {

                    if (!this.tracking_id) {
                        swal('Warning', 'Enter Member Tracking ID for this Process', 'warning');
                        return false;
                    }

                    INGENIOUS.blockUI(true);

                    this.$http.post('{{ route("store-api-get-user-details") }}', {
                        tracking_id: this.tracking_id,
                    }).then(response => {

                        INGENIOUS.blockUI(false);

                        if (response.data.status) {
                            this.user = response.data.user;
                        }
                        else {
                            swal('Oops', response.data.message, 'error');
                        }

                    });

                },
                setProductToOrder: function (selected_item_id) {

                    let item_exists = _.chain(this.order_items).filter(order_item => {
                        return parseInt(order_item.id) === parseInt(selected_item_id)
                    }).head().value();

                    if (!item_exists) {

                        let selected_product = _.chain(this.items).filter(item => {
                            return parseInt(item.id) === parseInt(selected_item_id)
                        }).head().value();

                        if (selected_product) {
                            selected_product.selected_qty = 1;
                            this.order_items.push(selected_product);
                        }
                    }
                    else {

                        _.map(this.items, item => {
                            if (parseInt(item.id) === parseInt(selected_item_id))
                                item.selected_qty += 1;
                            return item;
                        });

                    }

                },
                searchProduct: function (barcode) {

                    if (!this.user) {
                        swal('Alert', 'First Enter User ID to Start Billing', 'warning');
                        return false;
                    }

                    let scan_product = _.chain(this.items).filter(item => {
                        return parseInt(item.barcode) === parseInt(barcode)
                    }).head().value();

                    if(!scan_product){
                        swal('Alert', 'No Product Available for scanned Barcode', 'warning');
                        return false;
                    }
                    this.setProductToOrder(scan_product.id);
                },
                removeItem: function (index) {

                    let self = this;
                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to remove this item ?',
                        icon: "info", buttons: true, dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {
                            self.$delete(self.order_items, index);
                            swal('Removed', 'Item has been removed form List', 'success');
                        }
                    });
                },
                calculateSummary: function () {

                    this.order.amount = _.sumBy(this.order_items, item => {
                        return parseFloat((item.selected_qty*item.selling_price).toFixed(2));
                    });

                    this.order.total_bv = _.sumBy(this.order_items, item => {
                        return parseFloat((item.selected_qty*item.bv).toFixed(2));
                    });

                    this.order.total_qty = _.sumBy(this.order_items, item => {
                        return parseInt(item.selected_qty);
                    });

                    this.order.total_amount = this.order.amount;

                },
                createOrder: function () {

                    let stock_not_exists = _.chain(this.order_items).filter(order_item => {
                        return parseInt(order_item.selected_qty) > parseInt(order_item.balance)
                    }).head().value();

                    if (stock_not_exists) {
                        swal('Stock Warning', stock_not_exists.name + ' has not enough Stock, Current Stock: ' + stock_not_exists.balance, 'warning');
                        return false;
                    }

                    let self = this;

                    swal({
                        title: "Are you sure?",
                        text: 'Are you confirm to Create Order?',
                        icon: "info",
                        buttons: true,
                        dangerMode: true
                    }).then( function (isConfirm) {

                        if (isConfirm) {

                            INGENIOUS.blockUI(true);

                            self.$http.post('{{ route("store-order-create") }}', {
                                order_items: self.order_items,
                                order_details: self.order,
                                user_id: self.user.id,
                                offer_id: self.qualifiedOffer ? self.qualifiedOffer.id : null,
                                payment_mode: self.payment_mode,
                                reference_number: self.reference_number
                            }).then(response => {

                                if (response.data.status) {
                                    window.location = response.data.route;
                                }
                                else {
                                    INGENIOUS.blockUI(false);
                                    swal('Oops', response.data.message, 'error');
                                }

                            });

                        }

                    });
                },
                /* Offer Methods */
                checkOffer: function () {

                    /* Priority is Offer Type 2 */
                    let qualifiedOffer = null;
                    let condition_item_exists = null;

                    let productToProductExists = _.chain(this.activeOffers).filter(active_offer => {
                        return parseInt(active_offer.type) === 2
                    }).value();

                    if (productToProductExists.length > 0)
                    {
                        _.map(productToProductExists, p2POffer => {

                            let condition_item = p2POffer.details.condition_items[0];

                            if (!qualifiedOffer) {

                                condition_item_exists = _.chain(this.order_items).filter( item => {
                                    return parseInt(item.id) === parseInt(condition_item.product_price_id) && parseInt(item.selected_qty) >= parseInt(condition_item.qty);
                                }).head().value();

                                if (condition_item_exists && !qualifiedOffer) {
                                    condition_item_exists.required_qty = condition_item.qty;
                                    qualifiedOffer = p2POffer;
                                }
                            }

                        });
                    }

                    /* Offer Type 2 is not Exists then go for Other Offer */
                    if (!qualifiedOffer) {

                        qualifiedOffer = _.chain(this.activeOffers).filter(active_offer => {
                            return parseInt(active_offer.qualified) === 0 && parseFloat(this.order.total_amount) >= parseFloat(active_offer.min_amount) && parseFloat(this.order.total_amount) <= parseFloat(active_offer.max_amount);
                        }).head().value();

                        console.log(qualifiedOffer);

                    }

                    if (qualifiedOffer)
                    {
                        _.map(this.activeOffers, activeOffer => {
                            if (parseInt(activeOffer.id) === parseInt(qualifiedOffer.id)) {
                                activeOffer.qualified = 1;
                            }
                            return activeOffer;
                        });

                        this.qualifiedOffer = qualifiedOffer;

                        INGENIOUS.blockUI(true);
                        this.$http.post('{{ route("store-order-load-offer-items") }}', {
                            offer_id: this.qualifiedOffer.id, condition_item: condition_item_exists
                        }).then(response => {

                            if (response.data.status) {

                                setTimeout(() => {
                                    INGENIOUS.blockUI(false);
                                    swal("Offer Applied", this.qualifiedOffer.name +  '. Click on Place Order for Order', "success");
                                }, 1000);

                                if (parseInt(this.qualifiedOffer.type) !== 3) {

                                    let offer_items = _.chain(response.data.items).groupBy('group_id').toArray().value();

                                    if (offer_items.length > 1) {

                                        this.offer_item_options = offer_items;

                                        $('#offerItemSelectionModal').modal({
                                            backdrop: 'static', keyboard: false
                                        });

                                    }
                                    else {
                                        this.order_items = this.order_items.concat(response.data.items);
                                    }
                                }
                                else {

                                    this.order_items = this.order_items.concat(response.data.items);
                                }

                            }
                            else {
                                INGENIOUS.blockUI(false);
                                swal("Oops", response.data.message, "error");
                            }
                        })

                    }
                    else {
                        this.createOrder();
                    }

                },
                selectOfferOption: function (selected_items) {

                    $('#offerItemSelectionModal').modal('hide');
                    INGENIOUS.blockUI(true);
                    setTimeout(() => {
                        this.offer_item_options = [];
                        this.order_items = this.order_items.concat(selected_items);
                        INGENIOUS.blockUI(false);
                    }, 500);
                },
                removeOffer: function () {

                    if (this.qualifiedOffer)
                    {
                        INGENIOUS.blockUI(true);

                        setTimeout(() => {
                            INGENIOUS.blockUI(false);
                        }, 1000);

                        /* Remove exists Offered Item */
                        this.order_items = _.chain(this.order_items).reject(item => {
                            return item.offer_id;
                        }).value();

                        _.map(this.activeOffers).map(activeOffer => {

                            if (parseInt(activeOffer.id) === parseInt(this.qualifiedOffer.id)) {
                                activeOffer.qualified = 0;
                            }
                            return activeOffer;
                        });

                        this.qualifiedOffer = null;
                    }

                }
            }
        });

        function barcodeScanner() {

            /*
             * Barcode Scanner
             * Window Key event for Barcode Scanner which supports only
             * */
            $(function() {
                let chars = [];
                let barcode = '';
                let pressed = false;

                $(window).keydown(function(event) {

                    if(event.which === 13) {
                        event.preventDefault();
                        return false;
                    }

                    if (event.which >= 48 && event.which <= 57 && event.target.localName !== 'input') {
                        chars.push(String.fromCharCode(event.which));

                        if(pressed === false)
                        {
                            setTimeout( function () {

                                if(chars.length === 13) {
                                    barcode = chars.join('');
                                    createOrder.searchProduct(barcode);
                                }
                                else{
                                    swal('Oops..!!','Invalid Product Barcode, Please Try again','error')
                                }
                                chars = [];
                                pressed = false;
                            }, 500);
                        }
                        pressed = true;
                    }
                });

            });
        }
    </script>
@stop