@extends('store-manager.template.layout')

@section('title')
    My Store Orders
@stop

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Store Orders</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="la la-search"></i></span>
                                            </div>
                                            <input type="text" placeholder="Search Order..." name="search" class="form-control" value="{{ Request::get('search') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                            <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary"> Search </button>
                                        @refreshBtn('store')
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Order Date</th>
                                        <th>Order Id</th>
                                        <th>Customer</th>
                                        <th>Total BV</th>
                                        <th>Total Amount</th>
                                        <th>Details</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $index => $order)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($orders, $index) }}</td>
                                            <td>{{ $order->created_at->format('M d, Y h:i A') }}</td>
                                            <td>{{ $order->customer_order_id }}</td>
                                            <td>{{ $order->user->detail->full_name }} <br> ({{ $order->user->tracking_id }})</td>
                                            <td>{{ $order->total_bv }}</td>
                                            <td>{{ $order->total }}</td>
                                            <td>
                                                <a href="{{ route('store-order-detail', ['customer_order_id' => $order->customer_order_id]) }}" class="badge badge-primary">Details</a>
                                                @if(in_array($order->status, [\App\Models\Order::APPROVED]))
                                                    <a class="badge badge-danger" href="{{ route('store-order-detail', ['customer_order_id' => $order->customer_order_id, 'download' => 'receipt']) }}">Invoice
                                                    </a>
                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{ $orders->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

