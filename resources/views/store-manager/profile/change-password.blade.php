@extends('store-manager.template.layout')

@section('title', 'Change Password')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Change Password</h3>
        </div>
    </div>

    <div class="content-body">

        <section class="basic-inputs">
            <form role="form" method="post" onsubmit="INGENIOUS.blockUI(true)">
                {{ csrf_field() }}
                <div class="row match-height">
                    <div class="col-md-6 offset-md-3 offset-lg-3">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Password</h4>
                            </div>

                            <div class="card-block">
                                <div class="card-body">
                                    <label for="old_password">Old Password</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control passwordInput" name="old_password" placeholder="Enter Old Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="INGENIOUS.showPassword(this)">
                                                <i class="la la-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <label for="new_password" class="mt-2">New Password</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control passwordInput" name="new_password" placeholder="Enter New Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="INGENIOUS.showPassword(this)">
                                                <i class="la la-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center mt-2 mb-3">
                                    <button class="btn btn-danger" type="submit">Update Password</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
    </div>
@stop