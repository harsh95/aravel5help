@extends('store-manager.template.layout')

@section('title')
    Supply to Other Store History
@stop

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">Supply to Other Store History</h3>
        </div>
    </div>
    <div class="content-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form action="" method="get">
                                <div class="row mb-2">
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-calendar"></i></span></div>
                                            <input type="text" name="dateRange" class="form-control date-range" value="{{ Request::get('dateRange') }}" placeholder="Date range for Filter" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary"> Search </button>
                                        @refreshBtn('store')
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Created At</th>
                                        <th>Store</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($supplies) == 0)
                                        <tr>
                                            <td colspan="10" class="text-center">No Supplies Created</td>
                                        </tr>
                                    @endif
                                    @foreach ($supplies as $index => $supply)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($supplies, $index) }}</td>
                                            <td>{{ $supply->created_at->format('M d, Y h:i A') }}</td>
                                            <td>
                                                <span class="text-primary">{{ $supply->store->name }} <br> ({{ $supply->store->tracking_id }})</span>
                                            </td>
                                            <td>{{ number_format($supply->total, 2) }}</td>
                                            <td>
                                                @if($supply->status == 1)
                                                    <span class="badge badge-warning">Pending</span>
                                                @else
                                                    <span class="badge badge-success">Delivered</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('store-supply-detail', ['id' => $supply->id ]) }}" class="btn btn-danger btn-sm"> Details</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                                {{ $supplies->appends(['dateRange' => Request::get('dateRange')])->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop