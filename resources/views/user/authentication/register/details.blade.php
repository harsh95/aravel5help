@extends('user.template.registration.layout')

@section('title', 'Member / User Details for Registration')

@section('content')
    <div class="pt-2 pb-3">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-8 col-12 box-shadow-2 p-0">
                <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                    {{ csrf_field() }}
                    <div class="card border-grey border-lighten-3 px-1 py-1">
                        <div class="card-header border-0">
                            <div class="text-center mb-0">
                                <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}">
                            </div>
                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @elseif(session('errors'))
                                <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                            @endif
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    Sponsor Upline: <span class="pull-right">{{ $sponsor_user->detail->full_name }} ({{ $sponsor_user->tracking_id }})</span>
                                </li>
                            </ul>
                            <h4 class="card-title text-center mt-1">
                                Select the Correct Organisation of your Upline where you wish to be placed.
                            </h4>
                            <div class="row">
                                <div class="col-12 text-center">
                                    <div class="radio radio-danger mt-1" {{ isset($selected_leg) ? "style=pointer-events:none;" : ''}}>
                                        <input type="radio" value="L" name="leg" id="leftLeg" {{ old('leg') == 'L' || $selected_leg == 'L' ? 'checked' : '' }}>
                                        <label for="leftLeg">Left Team</label>
                                        <input type="radio" value="R" name="leg" id="rightLeg" {{ old('leg') == 'R' || $selected_leg == 'R' ? 'checked' : null }}>
                                        <label for="rightLeg">Right Team</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div class="card-header border-0">
                            <div class="font-medium-4 text-center">
                                Enter New User / Member Details
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <fieldset class="form-group">
                                        <label>Title</label>
                                        <select name="title" class="form-control square">
                                            <option value="Mr" {{ old('title') == 'Mr' ? 'selected' : null }}>Mr</option>
                                            <option value="Miss" {{ old('title') == 'Miss' ? 'selected' : null }}>Miss</option>
                                            <option value="Mrs" {{ old('title') == 'Mrs' ? 'selected' : null }}>Mrs</option>
                                            <option value="Mrs" {{ old('title') == 'Ms' ? 'selected' : null }}>Ms</option>
                                            <option value="Dr" {{ old('title') == 'Dr' ? 'selected' : null }}>Dr</option>
                                            <option value="Sri" {{ old('title') == 'Sri' ? 'selected' : null }}>Sri</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control square" name="first_name" placeholder="First Name" value="{{ old('first_name') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-5">
                                    <fieldset class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control square" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" autocomplete="off">
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset class="form-group">
                                        <label>Email</label>
                                        <input type="email" class="form-control square" name="email" placeholder="Email" value="{{ old('email') }}" autocomplete="off" >
                                    </fieldset>
                                </div>

                                <div class="col-md-4">
                                    <label>Birth Date</label>
                                    <div class="input-group">
                                        <input type='text' class="form-control square birth-date" name="birth_date" value="{{ old('birth_date') }}" placeholder="Birth date" readonly>
                                        <div class="input-group-append">
										<span class="input-group-text">
											<span class="la la-calendar-o"></span>
										</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Enter Address Details
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="form-group">
                                        <label>Your Address</label>
                                        <textarea class="form-control square" name="address" rows="6" placeholder="Enter Address">{{ old('address') }}</textarea>
                                    </fieldset>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label>Country</label>
                                                <select onchange="get_country()" class="form-control" name="country_id" id="country_id" required>
                                                    <option>Select Country</option>
                                                    @foreach($countries as $country)
                                                        <option value="{{ $country->id }}" {{ $country->id == old('country_id') ? 'selected' : '' }}>
                                                            {{ $country->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>City</label>
                                                <input type="text" class="form-control square" name="city" value="{{ old('city') }}" autocomplete="off">
                                            </fieldset>
                                            <fieldset class="form-group state">
                                                <label>State</label>
                                                <select name="state_id" id="state_id" class="form-control square">
                                                    <option value="">Select</option>
                                                    @foreach($states as $state)
                                                        <option value="{{ $state->id }}" {{ $state->id == old('state_id') ? 'selected' : '' }}>
                                                            {{ $state->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </fieldset>

                                        </div>
                                        <div class="col-md-6">
                                            <fieldset class="form-group">
                                                <label>District</label>
                                                <input type="text" class="form-control square" name="district" value="{{ old('district') }}" autocomplete="off">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label>Pincode</label>
                                                <input type="text" class="form-control square" onkeypress="INGENIOUS.numericInput(event)" name="pincode" value="{{ old('pincode') }}" autocomplete="off">
                                            </fieldset>


                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label>ISD Code</label>
                                            <input  readonly type="text" id="isd_code" name="isd_code" class="form-control" placeholder="Code">
                                        </div>
                                        <div class="col-md-8">
                                            <label>Mobile Number</label>
                                            <input type="text" class="form-control square" onkeypress="INGENIOUS.numericInput(event)" name="mobile" value="{{ old('mobile') }}" placeholder="10 Digit Mobile Number" autocomplete="new-mobile">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row pancard_div">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Enter Pan Card Details
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="checkbox check-danger  panCardCheckBox">
                                                <input type="checkbox" id="apply_pan" name="apply_pan" class="applyPanCard" onclick="applyPanCardCheckbox()" value="1">
                                                <label for="apply_pan">
                                                    A/F Pan Card Inquiry
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <fieldset class="form-group  panCardInputBox">
                                                <label>Pan Card</label>
                                                <input type="text" class="form-control square pan_card_input" name="pan_no" value="{{ old('pan_no') }}" autocomplete="off" onkeyup="panCardInputBox()">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-4">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row nagrikta_card_div">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Nagrikta Card Details
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">

                                        <div class="col-md-4 offset-md-4">
                                            <fieldset class="form-group  panCardInputBox">
                                                <label>Nagrikta Card No.</label>
                                                <input type="text" name="nagrikta_card_no" class="form-control square nagrikta_card_input" value="{{ old('nagrikta_card_no') }}" autocomplete="off">
                                            </fieldset>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="font-medium-4 text-center mb-1">
                                        Account Details
                                    </div>
                                </div>
                                <div class="col-md-6 offset-3">
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input type="password" class="form-control passwordInput" name="password" placeholder="Enter Password">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button" onclick="INGENIOUS.showPassword(this)">
                                                <i class="la la-eye-slash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mt-1">
                                    <div class="checkbox check-danger">
                                        <input type="checkbox" id="terms" name="terms" required>
                                        <label for="terms">
                                            I agree to the Terms and Conditions and Privacy Policy For {{ config('project.company') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center mt-1">
                                    <button type="submit" class="btn round btn-glow btn-bg-gradient-x-red-pink col-6 mr-1 mb-1">Continue</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('page-javascript')
    <script>
        $(document).ready(function () {
           var country = $("#country_id").val();
            $(".nagrikta_card_div").hide();
            $(".pancard_div").hide();
           if(country == 99)
           {
               $(".nagrikta_card_div").hide();
               $(".pancard_div").show();
           }
           if(country == 148){
               $(".nagrikta_card_div").show();
               $(".pancard_div").hide();
           }
        });
        $(".birth-date").pickadate({
            selectMonths: !0,
            selectYears: 50,
            format: 'dd-mmm-yyyy',
            max: new Date('{{ \Carbon\Carbon::now()->subYears(18)->format('Y,m,d') }}')
        });

        function applyPanCardCheckbox(){
            if($('.applyPanCard').is(":checked")){
                $('.panCardInputBox').hide();
            }else{
                $('.panCardInputBox').show();
            }
        }
        function panCardInputBox(){
            if($('.pan_card_input').val() != '')
            {
                $('.panCardCheckBox').hide();
            }
            else if( $('.pan_card_input').val() == '' ){
                $('.panCardCheckBox').show();
            }
        }

        function get_country() {

            $(".state").show();
            $(".nagrikta_card_div").hide();
            $(".pancard_div").show();
            var country = $("#country_id").val();
            if(country == 148)
            {
                $("#isd_code").val('+977');
                $(".state").hide();
                $(".nagrikta_card_div").show();
                $(".pancard_div").hide();
            }
            if(country == 99)
            {
                $("#isd_code").val('+91');
            }
        }
        get_country();
    </script>
@stop