@extends('user.template.registration.layout')

@section('title', 'Registration')

@section('content')
    <section class="flexbox-container">
        <div class="col-12 d-flex align-items-center justify-content-center">
            <div class="col-md-4 col-12 box-shadow-2 p-0">
                <div class="card border-grey border-lighten-3 m-0">
                    <div class="card-header border-0">
                        <div class="text-center mb-1">
                            <img src="/user-assets/images/company/logo.png" alt="{{ config('project.brand') }}">
                        </div>
                        @if(session('error'))
                            <div class="alert alert-danger">{{ session('error') }}</div>
                        @elseif(session('errors'))
                            <div class="alert alert-danger">{{ session('errors')->first() }}</div>
                        @else
                            <div class="font-large-1  text-center">
                                Register at {{ config('project.brand') }}
                            </div>
                        @endif
                    </div>
                    <div class="card-content">

                        <div class="card-body">
                            <form class="form-horizontal" action="" method="post" novalidate>
                                {{ csrf_field() }}
                                <fieldset class="form-group position-relative has-icon-left">
                                    <input type="text" class="form-control round" name="tracking_id" placeholder="Enter Sponsor Upline Tracking ID" value="{{ $sponsor_upline ? $sponsor_upline->tracking_id : old('tracking_id') }}" required>
                                    <div class="form-control-position"><i class="ft-user"></i></div>
                                </fieldset>
                                <br>
                                <div class="form-group text-center">
                                    <button type="submit" class="btn round btn-block btn-glow btn-bg-gradient-x-purple-blue col-12 mr-1 mb-1">Next</button>
                                </div>
                            </form>
                        </div>
                        <p class="card-subtitle text-muted text-right font-small-3 mx-2 my-1">
                            <span>Already have an account ? <a href="{{ route('user-login') }}" class="card-link">Login</a></span>
                        </p>
                    </div>

                </div>

            </div>
        </div>
    </section>
@stop