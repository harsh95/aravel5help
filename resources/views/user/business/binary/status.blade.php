@extends('user.template.layout')

@section('title', 'My Binary Status')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Binary Status</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">

                                <thead>
                                <tr>
                                    <th rowspan="2">Close Date</th>
                                    <th colspan="10" class="text-center">Representatives</th>
                                </tr>
                                <tr>
                                    <th rowspan="1" class="text-center">Status</th>
                                    <th colspan="2" class="text-center bg-warning bg-darken-1 white">Total Associates</th>
                                    <th colspan="2" class="text-center bg-danger bg-darken-1 white">Total PV</th>
                                    <th colspan="2" class="text-center bg-teal bg-darken-1 white">Forward PV</th>
                                    <th colspan="2" class="text-center bg-blue bg-darken-1 white">New PV</th>
                                    <th class="text-center bg-purple bg-darken-1 white">Completed Ratio</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>Left</th>
                                    <th>Right</th>
                                    <th>Left</th>
                                    <th>Right</th>
                                    <th>Left</th>
                                    <th>Right</th>
                                    <th>Left</th>
                                    <th>Right</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $difference_left = 0; $difference_right = 0; @endphp
                                @foreach($binary_calculations as $index => $record)
                                    <tr>
                                        <td>{{ $record->closed_at ? Carbon\Carbon::parse($record->closed_at)->format('d-m-Y') : 'Current Status' }}</td>
                                        <td>
                                            @if($record->status == 0)
                                                <span class="badge badge-success"> Current </span>
                                            @elseif($record->status == 1)
                                                <span class="badge badge-primary"> Closed </span>
                                            @elseif($record->status == 2)
                                                <span class="badge badge-danger"> Rejected </span>
                                            @endif
                                        </td>
                                        <td class="bg-warning bg-lighten-4">{{ $record->left }}</td>
                                        <td class="bg-warning bg-lighten-4">{{ $record->right }}</td>
                                        <td class="bg-danger bg-lighten-4">{{ $record->total_left }}</td>
                                        <td class="bg-danger bg-lighten-4">{{ $record->total_right }}</td>
                                        <td class="bg-teal bg-lighten-4">{{ $record->forward_left }}</td>
                                        <td class="bg-teal bg-lighten-4">{{ $record->forward_right }}</td>
                                        <td class="bg-blue bg-lighten-4">{{ $record->current_left }}</td>
                                        <td class="bg-blue bg-lighten-4">{{ $record->current_right }}</td>
                                        <td class="text-center bg-purple bg-lighten-4">
                                            {{ ($record->forward_left+$record->current_left)-$difference_left }} :
                                            {{ ($record->forward_right+$record->current_right)-$difference_right }}
                                        </td>
                                        @php $difference_left = $record->forward_left; $difference_right = $record->forward_right; @endphp
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop