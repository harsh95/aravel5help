@extends('user.template.layout')

@section('title', 'Bank Detail - KYC Update')

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-6 col-12 mb-2">
            <h3 class="content-header-title">
                Bank Detail - KYC Update
            </h3>
        </div>
    </div>
    <div class="content-body">
        <form action="" method="post" enctype="multipart/form-data" onsubmit="INGENIOUS.blockUI(true)">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Bank Name</label>
                                    <input type="text" class="form-control square" placeholder="Bank Name" name="bank_name" value="{{ $user_bank->bank_name}}" {{ !$allow_update ? 'disabled' : null }}>
                                </div>

                                <div class="form-group">
                                    <label>Account Holder Name</label>
                                    <input type="text" class="form-control square" placeholder="Account Holder Name" name="account_name" value="{{ $user_bank->account_name }}" {{ !$allow_update ? 'disabled' : null }}>
                                </div>

                                <div class="form-group">
                                    <label>Account Number</label>
                                    <input type="text" class="form-control square" onkeypress="INGENIOUS.numericInput(event)" placeholder="Account Number" name="account_number" value="{{ $user_bank->account_number }}" {{ !$allow_update ? 'disabled' : null }}>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Branch</label>
                                            <input type="text" class="form-control square" placeholder="Bank Branch" name="branch" value="{{ $user_bank->branch }}" {{ !$allow_update ? 'disabled' : null }}>
                                        </div>
                                        <div class="form-group">
                                            <label>IFSC</label>
                                            <input type="text" class="form-control square" placeholder="IFSC Code" name="ifsc" value="{{ $user_bank->ifsc }}" {{ !$allow_update ? 'disabled' : null }}>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>City</label>
                                            <input type="text" class="form-control square" placeholder="Banks Branch City" name="city" value="{{ $user_bank->city }}" {{ !$allow_update ? 'disabled' : null }}>
                                        </div>
                                        <div class="form-group">
                                            <label>Account Type</label>
                                            <select class="form-control" name="account_type" {{ !$allow_update ? 'disabled' : null }}>
                                                <option value="1" {{ $user_bank->type == 1 ? 'selected' : '' }}>Saving</option>
                                                <option value="2" {{ $user_bank->type == 2 ? 'selected' : '' }}>Current</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if($allow_update)
                                    <div class="form-group">
                                        <input type="file" id="imageUploader" name="image" data-allowed-file-extensions="jpg png jpeg" data-max-file-size="4M" />
                                    </div>
                                    <div class="form-group">
                                        <label>Document Type</label>
                                        <select class="form-control" name="type">
                                            @foreach(\App\Models\UserDocument::documents('Bank') as $index => $bank_document)
                                                <option value="{{ $index }}" {{ $index == old('type') ? 'selected' : '' }}>{{ $bank_document }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                @if(count($documents) > 0)
                                    <hr>
                                    <table class="table table-bordered table-responsive">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Account Number</th>
                                            <th>Image</th>
                                            <th>Status</th>
                                            <th>Remarks</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($documents as $index => $document)
                                            <tr>
                                                <td>{{ $index +1 }}</td>
                                                <td>{{ $document->created_at->format('M d, Y h:i A') }}</td>
                                                <td>{{ $document->number }}</td>
                                                <td>
                                                    <a href="{{ env('DOCUMENT_IMAGE_URL') . $document->image  }}" target="_blank" class="btn btn-facebook btn-sm">View Image</a>
                                                    <br>
                                                    <code>{{ $document->document_name }}</code>
                                                </td>
                                                <td>
                                                    @if($document->status == \App\Models\UserDocument::PENDING)
                                                        <span class="badge bg-pink"><i class="la la-exclamation-triangle"></i> In Progress</span>
                                                    @elseif($document->status == \App\Models\UserDocument::VERIFIED)
                                                        <span class="badge badge-success"><i class="la la-check-circle"></i> Verified </span>
                                                    @else
                                                        <span class="badge badge-danger"><i class="la la-close"></i> Rejected</span>
                                                        <br>
                                                        <small>{{ $document->remarks }}</small>
                                                    @endif
                                                </td>
                                                <td>{{ $document->remarks }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                @if($allow_update)
                                    <button type="submit" class="btn btn-danger">Upload</button>
                                    <br>
                                    <small class="text-danger">Jpg, png, jpeg Image Formats are allowed & Maximum Size is 4 MB</small>
                                @else
                                    <h5 class="text-danger">Your Bank KYC Details is Under Reviewed, Will Update Soon</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop

@section('page-javascript')
    <script>
        $('#imageUploader').dropify({
            messages: {'default': 'Upload Your Cancel Cheque or Pass Book Image'},
            tpl: {
                filename: '<p class="dropify-filename">Cancel Cheque or Pass Book</p>',
            }
        });
    </script>
@stop

@section('import-javascript')
    <script src="/plugins/dropify/dropify.min.js"></script>
@stop

@section('import-css')
    <link href="/plugins/dropify/dropify.min.css" type="text/css" rel="stylesheet" />
@stop