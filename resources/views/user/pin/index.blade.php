@extends('user.template.layout')

@section('title', 'My Pins')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">My Pins</h3>
        </div>
    </div>
    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <form action="" method="get">
                            <div class="row mb-2">
                                <div class="col-md-4">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="la la-search"></i></span>
                                        </div>
                                        <input type="text" placeholder="Search Pin Number" name="search" class="form-control" value="{{ Request::get('search') }}">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    @refreshBtn('user')
                                </div>
                            </div>
                        </form>
                        <form action="" method="post" onsubmit="INGENIOUS.blockUI(true)">
                            {{ csrf_field() }}
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>
                                            <a href="javascript:void(0)" class="badge badge-danger mb-1" data-toggle="modal" data-keyboard="false" data-target="#pinTransferModal">Transfer</a>
                                            <div class="checkbox check-danger">
                                                <fieldset>
                                                    <input type="checkbox" id="parentCheck" class="parentCheckBox">
                                                    <label for="parentCheck">Select All</label>
                                                </fieldset>
                                            </div>
                                        </th>
                                        <th>Created On</th>
                                        <th>Pin Number</th>
                                        <th>Package</th>
                                        <th>Used By</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pins as $index => $pin)
                                        <tr>
                                            <td>{{ \App\Library\Helper::tableIndex($pins, $index) }}</td>
                                            <td>
                                                @if ($pin->status == 0)
                                                    <div class="checkbox check-danger">
                                                        <fieldset>
                                                            <input type="checkbox" id="check{{ $index }}" name="pin_ids[]" value="{{ $pin->id }}" class="childCheckBox">
                                                            <label for="check{{ $index }}"></label>
                                                        </fieldset>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>{{ $pin->created_at->format('M d, Y h:i A') }}</td>
                                            <td>
                                                <a href="javascript:void(0)" title="Click to Copy" class="pin-element" data-clipboard-text="{{ $pin->number }}">
                                                    {{ $pin->number }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $pin->package->name }} <br> (Rs. {{ $pin->amount }})
                                            </td>
                                            <td>
                                                @if($pin->status == 1 && $pin->package_order)
                                                    {{ $pin->package_order->user->detail->full_name }}
                                                    ({{ $pin->package_order->user->tracking_id }})
                                                    <br>
                                                    <small class="text-primary">
                                                        Date:  {{ $pin->package_order->created_at->format('M d, Y') }}
                                                    </small>
                                                @endif
                                            </td>
                                            <td>
                                                @if($pin->status == 0)
                                                    <span class="badge badge-success">Unused</span>
                                                @elseif($pin->status == 1)
                                                    <span class="badge badge-danger">Used</span>
                                                @else
                                                    <span class="badge badge-warning">Block</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $pins->links() }}
                            </div>

                            <div class="modal fade text-left" id="pinTransferModal" tabindex="-1" role="dialog" aria-labelledby="basicModalLabel3" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="basicModalLabel3">Transfer the Pins</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <fieldset class="form-group">
                                                <label>Enter Tracking ID</label>
                                                <input type="text" class="form-control square" onchange="INGENIOUS.getDownlineUser(this)" placeholder="Enter Downline Tracking ID">
                                            </fieldset>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger transferPins">Transfer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('page-javascript')
    <script>

        $(function () {

            $('.parentCheckBox').click(function() {
                $('.childCheckBox').prop('checked', this.checked);
            })

        });

        var btns = document.querySelectorAll('.pin-element');

        var clipboard = new ClipboardJS(btns);
        clipboard.on('success', function(e) {
            toastr.success('Pin has been copied..!!', "Done..!!");
        });
        clipboard.on('error', function(e) {
            toastr.error('Error while copying pin', "Error");
        });

    </script>
@stop