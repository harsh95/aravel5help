@php
    $f = new \App\Library\CurrencyInWord;
@endphp
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $filename }}</title>
    <style>
        @media print {
            @page  {
                margin: .5cm .3cm;
            }
        }
        table { page-break-inside:auto }
        tr    { page-break-inside:avoid; page-break-after:auto;}
        thead { display:table-header-group }
        tfoot { display:table-footer-group }
        body {
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }
        .text-center {
            text-align: center !important;
        }
        .text-right {
            text-align: right !important;
        }
        .datagrid table {
            border-collapse: collapse;
            text-align: left;
            width: 100%;
        }

        .datagrid {
            font: normal 12px/150% Verdana, Arial, Helvetica, sans-serif;
            background: #fff;
            overflow: hidden;
            border: 0px solid #000;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .datagrid table td,
        .datagrid table th {
            padding: 3px 10px;
        }

        .datagrid table tbody tr.header td {
            filter: progid: DXImageTransform.Microsoft.gradient(startColorstr='#FFFAC3', endColorstr='#80141C');
            background-color: #000;
            color: #FFFFFF;
            font-size: 10px;
            font-weight: bold;
            padding: 7px;
        }
        .datagrid table tbody tr.table-title td
        {
            text-align: center;
            background: #3180C2;
            color: #fff;
            font-size: 12px !important;
            border-top: 2px solid #000;
            padding: 7px;
            font-weight: 600;
        }
        .datagrid table tbody tr.child-category-title td
        {
            font-size: 13px;
        }
        .datagrid table thead th:first-child {
            border: none;
        }

        .datagrid table tbody td {
            color: #000;
            border: 1px solid #000;
            font-size: 10px;
            font-weight: normal;
            padding: 8px;
            border-bottom: 1px solid #000;
        }
        .datagrid table tbody .alt td {
            background: #F7CDCD;
            color: #000;
        }
        /*.datagrid table tbody td:first-child {*/
        /*border-left: none;*/
        /*}*/

        /*.datagrid table tbody tr:last-child td {*/
        /*border-bottom: none;*/
        /*}*/
        .border-bottom {
            border-bottom: 1px solid #000 !important;
        }
    </style>
</head>
<body>

<table style="width: 100%">
    <tr>
        <td width="50%" style="text-align: left">
            @if($order->store)
                <div style="font-size: 16px; margin-top: 10px;font-weight: 800; color: #3180C2">
                    {{ $order->store->name }} ({{ $order->store->tracking_id }})</div>
                <div class="address" style="font-size: 14px; color: #122b40;">
                    <div style="font-size: 14px; color: #122b40;"> Address: </div>
                    {{ $order->store->address }}, {{ $order->store->city }} <br>
                    {{ $order->store->state->name }} - {{ $order->store->pincode }} <br>
                    GST No: N.A<br>
                </div>
            @else
                <div style="font-size: 16px; margin-top: 10px;font-weight: 800; color: #3180C2">
                    ARASIS HEALTH & BEAUTY</div>

                <div class="address" style="font-size: 14px; color: #122b40;">
                    <div style="font-size: 14px; color: #122b40;"> Address: </div>
                    Dhukuria Near G D Goenaka Public School, Soliguri <br>
                    Darajeeling, West Bengal - 734009,  INDIA <br>
                    GST No: 19ABVFA2300B12T<br>
                </div>
            @endif
        </td>
        <td>
            <div class="text-uppercase" style="font-size: 16px; font-weight: 600;margin-top: 100px; text-transform: uppercase;">RETAIL INVOICE</div>
        </td>
        <td width="50%" style="text-align: right">
            <img src="{{ config('project.url') }}user-assets/images/company/logo.png" alt="{{ config('project.brand') }}" class="mb-3 pull-right" >
        </td>

    </tr>
</table>

<div class="datagrid" style="margin-top: 15px;">
    <table>
        <tbody>
        <tr>
            <td width="50%">
                <div style="font-size: 15px;"> Invoice No : <span>{{ $order->customer_order_id }}</span> </div>
            </td>
            <td class="50%">
                <div style="font-size: 15px;"> CNI No : <span> </span></div>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <div style="font-size: 15px;">Invoice Date : {{ \Carbon\Carbon::parse($order->created_at)->format('M d, Y h:i A') }}<span></span></div>
            </td>
            <td class="50%">
                <div style="font-size: 15px;"> Vehicle No : <span>N.A</span></div>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <div style="font-size: 15px;"> Is Tax Payable On Reverse Charge? : No </div>
            </td>
            <td class="50%">
                <div style="font-size: 15px;"> PAN No :</div>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <div style="font-size: 15px; text-align: center; font-weight: bold">(Bill To) </div>
            </td>
            <td class="50%">
                <div style="font-size: 15px;text-align: center; font-weight: bold">(Shipped To)</div>
            </td>
        </tr>
        <tr>
            <td width="50%">
                <div style="font-size: 15px;">
                    Name :
                    <span style="margin-left: 5em">{{ $order->user->detail->full_name }} ({{ $order->user->tracking_id }})</span>
                </div>
                <div style="font-size: 15px;">Contact :
                    <span style="margin-left: 4.2em">{{ $order->user->mobile}}</span>
                </div>
                <div style="font-size: 15px;">Address :
                    <span style="margin-left: 4em">
                     {{ $order->user->address ? $order->user->address->address : 'N.A' }}
                    </span>
                </div>
                <div style="font-size: 15px;">State :
                    <span style="margin-left: 5.3em">{{ $order->user->address ? $order->user->address->state->name : 'N.A' }}</span>
                </div>
                <div style="font-size: 15px; margin-top: 2em;font-weight: bold">GSTIN No :
                    <span style="margin-left: 3em">
                        N.A
                    </span>
                </div>
            </td>
            <td width="50%">
                <div style="font-size: 15px;">
                    Name :
                    <span style="margin-left: 5em">{{ $order->user->detail->full_name }} ({{ $order->user->tracking_id }})</span>
                </div>
                <div style="font-size: 15px;">Contact :
                    <span style="margin-left: 4.2em">{{ $order->user->mobile}}</span>
                </div>
                <div style="font-size: 15px;">Address :
                    <span style="margin-left: 4em">
                     {{ $order->user->address ? $order->user->address->address : 'N.A' }}
                    </span>
                </div>
                <div style="font-size: 15px;">State :
                    <span style="margin-left: 5.3em">{{ $order->user->address ? $order->user->address->state->name : 'N.A' }}</span>
                </div>
                <div style="font-size: 15px; margin-top: 2em;font-weight: bold">GSTIN No :
                    <span style="margin-left: 3em">
                        N.A
                    </span>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <table style="width: 100%;">
        <tbody>
        @php
            $sum = 0;
            $total = 0;
        @endphp
        <tr class="text-center">
            <td colspan="8"></td>
            @if($order->user->address->state_id == 34)
                <td colspan="2">CGST</td>
                <td colspan="2">SGST</td>
            @else
                <td colspan="2">IGST</td>
            @endif
            <td colspan="6"></td>
        </tr>
        <tr class="table-title">
            <td width="3%" rowpan="8">S. No.</td>
            <td width="15%" rowpan="8">Product Name</td>
            <td width="5%" rowpan="8">HSN Code</td>
            <td width="5%" rowpan="8">Qty</td>
            <td width="8%" rowpan="8">Rate</td>
            <td width="8%" rowpan="8">Total</td>
            <td width="8%" rowpan="8">Discount</td>
            <td width="8%" rowpan="8">Taxable Value</td>
            @if($order->user->address->state_id == 34)
                <td width="3%" rowpan="8">%</td>
                <td width="3%" rowpan="8">Amt</td>
                <td width="3%" rowpan="8">%</td>
                <td width="3%" rowpan="8">Amt</td>
            @else
                <td width="3%" rowpan="8">%</td>
                <td width="3%" rowpan="8">Amt</td>
            @endif
            <td width="10%">Total</td>
            <td width="5%" rowpan="8">BV</td>
        </tr>

        @foreach($order->details as $index => $detail)
            <tr class="text-center">
                <td>{{ $index+1 }}</td>
                <td>{{ $detail->product_price->product->name }}</td>
                <td>{{ $detail->gst->code }}</td>
                <td>{{ $detail->qty }}</td>
                <td>
                    {{ number_format($detail->actual_amount, 2) }}
                </td>
                <td>
                    {{ number_format($detail->actual_amount * $detail->qty, 2) }}
                </td>
                <td>0</td>
                <td>{{ number_format($detail->total_taxable_amount, 2) }}</td>
                @if($order->user->address->state_id == 34)
                    <td>{{ $detail->gst->percentage/2 }}%</td>
                    <td>{{ number_format(($detail->total_tax_amount)/2, 2) }} </td>

                    <td>{{ $detail->gst->percentage/2 }}%</td>
                    <td>{{ number_format(($detail->total_tax_amount)/2, 2) }} </td>
                @else
                    <td>{{ $detail->gst->percentage }}%</td>
                    <td>{{ number_format(($detail->total_tax_amount), 2) }} </td>
                @endif
                <td>{{ number_format(($detail->total_amount), 2) }}</td>
                <td>{{ $detail->total_bv }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <table style="width:100%;border-right: 1px solid #000;">
        <tbody>
        <tr>
            <td class="33%" colspan="3">
                <div style="font-size: 15px;">
                    Before Discount Amount :
                    <span>{{ number_format(collect($order->details)->sum('total_taxable_amount'), 2) }}</span>
                </div>
            </td>
            <td class="33%">
                <div style="font-size: 15px;">
                    Discount :
                    <span>{{ number_format(collect($order->details)->sum('discount'), 2) }}</span>
                </div>
            </td>
            <td class="34%">
                <div style="font-size: 15px;">
                    Taxable Amount :
                    <span>{{ number_format(collect($order->details)->sum('total_taxable_amount'), 2) }}</span>
                </div>
            </td>
        </tr>
        <tr>
            @if($order->user->address->state_id == 34)
                <td class="33%">
                    <div style="font-size: 15px;">
                        CGST Amount :
                        <span>{{  number_format(collect($order->details)->sum('total_tax_amount')/2, 2)  }}</span>
                    </div>
                </td>
                <td class="33%" colspan="2">
                    <div style="font-size: 15px;">
                        SGST Amount :
                        <span>{{  number_format(collect($order->details)->sum('total_tax_amount')/2, 2)  }}</span>
                    </div>
                </td>
            @else

                <td class="33%" colspan="3">
                    <div style="font-size: 15px;">
                        IGST Amount :
                        <span>{{  number_format(collect($order->details)->sum('total_tax_amount'), 2)  }}</span>
                    </div>
                </td>
            @endif
            <td class="33%" >
                <div style="font-size: 15px;"><span></span></div>
            </td>
            <td class="34%">
                <div style="font-size: 15px;">
                    Total Tax(+) :
                    <span>{{  number_format(collect($order->details)->sum('total_tax_amount'), 2)  }}</span>
                </div>
            </td>
        </tr>
        <tr>
            <td class="33%" colspan="3">
                <div style="font-size: 15px;"><span></span></div>
            </td>
            <td class="33%" >
                <div style="font-size: 15px;">Total Qty : {{ collect($order->details)->sum('qty') }}<span></span></div>
            </td>
            <td class="34%" >
                <div style="font-size: 15px;">Round Off(+/-) : <span>0</span></div>
            </td>
        </tr>
        <tr>
            <td colspan="4" class="70%">
                <div style="font-size: 15px;">
                    Rs. (In Words) :
                    <b style="font-size: 15px;">
                        @if($order->wallet > 0)
                            {{ ucwords($f->display( collect($order->details)->sum('qty') * 1 )) }}
                        @else
                            {{ ucwords($f->display(collect($order->details)->sum('total_amount'))) }}
                        @endif
                    </b>
                </div>
            </td>
            <td class="30%">
                <div style="font-size: 15px;">
                    Net Amount :
                    @if($order->wallet > 0)
                        <span style="font-weight: 800;font-size: 16px">{{ number_format( collect($order->details)->sum('qty') * 1 )}}</span>
                    @else
                        <span style="font-weight: 800;font-size: 16px">{{ number_format(collect($order->details)->sum('total_amount') )}}</span>
                    @endif
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <div style="font-size: 18px;margin: 1.5rem 0 0.8rem 0; font-weight: 800">Invoice Details<span></span></div>

    <table  style="margin: 1rem 0 2rem 0; border: 1px solid #000;">
        <body>
        <tr>
            <td class="25%">
                <div style="font-size: 14px;font-weight: 800">
                    @if($order->wallet == 0)
                        Cash : <span style="font-weight: 800;"> {{number_format($order->total)}}</span>
                    @else
                        Payment - Offer Billing (Regular Buying Offer)
                    @endif
                </div>
            </td>
            <td class="20%" style="font-size: 14px;font-weight: 800">
                {{--Flat 20% discount on M.R.P--}}
            </td>
            <td class="20%" style="font-size: 14px;font-weight: 800">
                <div style="font-size: 12px;">
                    {{--<span > Running offer Discount </span>--}}
                </div>
            </td>
            <td width="20%" style="font-size: 14px;font-weight: 800">
                {{--Your Total Saving--}}
            </td>
        </tr>
        <tr >
            <td class="20%">
                <div style="font-size: 14px;font-weight: 800">Order BV : <span style="font-size: 14px;font-weight: 800">{{ $order->total_bv }}</span></div>
            </td>
            <td class="33%" colspan="2">
                {{--<div style="font-size: 12px;">Purchase BV : <span>{{ $order->total_bv }}</span></div>--}}
            </td>
            <td class="34%">
                {{--<div style="font-size: 12px;"><span>Bonus BV : {{ $order->total_bv }} </span></div>--}}
            </td>
        </tr>
        </body>
    </table>
    <table style="width: 100%;border-right: 1px solid #000;">
        <tbody>
        <tr class="50%">
            <td colspan="2">
                <div class="text-uppercase text-center" style="font-size: 15px;"><span>Terms & Condition Of Invoice </span></div>
            </td>
            <td class="50%">
                <div class="text-center" style="font-size: 15px;"><span> Certified that the particulars given above are true and correct</span></div>
            </td>
        </tr>
        <tr class="60%">
            <td rowspan="2" colspan="2">
                <p class="pull-left" style="font-size: 10px;"><span><b>We declare that this invoice shows the actual price of the goods.</b></span></p>
            </td>
            <td>
                <div class="text-center" style="font-size: 15px;">
                    <span>For <br>
                        @if($order->store)
                            {{ $order->store->name }} <br>({{ $order->store->tracking_id }})
                        @endif
                    </span></div>
                <br>
                <br>
                <br>
            </td>
        </tr>
        <tr class="40%">
            <td >
                <br>
                <br>
                <br>
                <div class="text-center" style="font-size: 15px;"><span>Authorized Signatory.</span></div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="pull-left" style="font-size: 15px;"><span>Receiver Signature:  </span></div>
            </td>
            <td>
                <div class="text-center"  style=" text-align: center; font-weight: 600; font-size: 15px;">THANK YOU FOR SHOPPING WITH US !</div>
            </td>
        </tr>
        </tbody>
    </table>
</div>



<div style="text-align: center; margin-top: 10px; font-size: 14px;">
    This is a Computer Generated Order Invoice
</div>
<div style="text-align: center; margin-top: 10px; font-size: 14px; border-top: 1px solid">
    **END OF INVOICE**
</div>
</body>
</html>