@extends('user.template.layout')

@section('title', 'User Support')

@section('content')

    <div class="content-header row">
        <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Support</h3>
        </div>
    </div>

    <div class="content-body">
        <section>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <form action="" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Support Category</label>
                                    <select name="category_id" class="form-control square">
                                        <option value="">Select</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3" style="margin-top: 2rem;">
                                    <button type="button" class="btn btn-block btn-glow btn-bg-gradient-x-purple-red openModal">Ask for Support</button>
                                </div>
                            </div>
                            <div class="modal" tabindex="-1" role="dialog" id="askSupport">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Enter Message for Support</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <h4 class="alert alert-warning text-center" role="alert">
                                                <span class="text-bold-300">Support For</span>
                                                <span class="selected_category text-bold-600"></span>
                                            </h4>
                                            <div class="form-group">
                                                <textarea name="message" class="form-control square" cols="30" rows="6" placeholder="Enter Message for Support" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <input type="file" name="image" id="supportImage" placeholder="Image for Support Chat">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-danger btn-glow btn-bg-gradient-x-purple-red">Submit</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Admin Manager</th>
                                    <th>Message</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($supports as $index => $support)
                                    <tr>
                                        <td>{{ \App\Library\Helper::tableIndex($supports, $index) }}</td>
                                        <td>{{ $support->created_at->format('M d, Y') }}</td>
                                        <td>{{ $support->category->name }}</td>
                                        <td>{{ $support->admin ? $support->admin->name : '-' }}</td>
                                        <td>{{ str_limit($support->message, 50) }}</td>
                                        <td>
                                            @if($support->status == \App\Models\Support::OPEN)
                                                <span class="badge bg-yellow text-dark">Open</span>
                                            @else
                                                <span class="badge bg-pink">Closed</span>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('user-support-chat', ['id' => $support->id]) }}" class="btn btn-sm btn-primary">
                                                Chat
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $supports->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop()

@section('page-javascript')
    <script>
        $('.openModal').click(function () {


            var category_id = $("select[name=category_id]").val();
            var category_name = $("select[name=category_id] option:selected").text();

            if (!category_id) {
                toastr.warning('Select the Support Category', "Warning");
                return false;
            }

            $('#askSupport').modal();
            $('.selected_category').text(category_name);

        });

        $('#supportImage').filer({ limit: 1, maxSize: 2, extensions: ['jpg', 'jpeg', 'png'], changeInput: true, showThumbs: true });
    </script>
@stop

@section('import-javascript')
    <script src="/user-assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>
@stop

@section('import-css')
    <link rel="stylesheet" href="/user-assets/plugins/jquery.filer/css/jquery.filer.css">
@stop