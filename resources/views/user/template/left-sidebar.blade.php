<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow " data-scroll-to-active="true" data-img="/user-assets/images/backgrounds/03.jpg">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto">
                <a class="navbar-brand" href="{{ route('user-dashboard') }}">
                    <img class="brand-logo" alt="{{ config('project.brand') }}" src="/user-assets/images/company/logo.png"/>
                </a>
            </li>
            <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
    </div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class="nav-item">
                <a href="{{ route('user-dashboard') }}">
                    <i class="ft-home"></i><span class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ Session::get('user')->referral_link }}" target="_blank">
                    <i class="ft-user-plus"></i><span class="menu-title">New Registration</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-reward-view') }}"><i class="ft-user-check bg-dark text-white"></i><span class="menu-title">Rewards</span></a>
            </li>
            <li class="nav-item">
                <a href="#"><i class="ft-layers"></i><span class="menu-title" data-i18n="">Business</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('user-business-sponsors') }}">My Sponsors</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-business-sponsors-tree') }}">Sponsor Level Tree</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-business-children', ['leg' => 'L']) }}">My Downline List</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-business-binary-tree') }}">Team Tree</a>
                    </li>
                    {{--<li>--}}
                        {{--<a class="menu-item" href="{{ route('user-business-binary-status') }}">Binary Status</a>--}}
                    {{--</li>--}}
                </ul>
            </li>

            <li class="nav-item">
                <a href="#"><i class="la la-shopping-cart"></i><span class="menu-title">Shopping</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('user-products-view') }}">Products</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-shopping-order-view') }}">Orders</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#"><i class="ft-user"></i><span class="menu-title">Account</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('user-account-profile') }}">Profile</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-account-identity-card') }}">ID Card</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-welcome-letter') }}">Welcome Letter</a>
                    </li>
                    <li>
                        <a href="{{ route('user-account-password') }}" class="menu-item">Change Password</a>
                    </li>

                </ul>
            </li>
            <li class="nav-item">
                <a href="#"><i class="la la-money"></i><span class="menu-title">My Income Bonus</span></a>
                <ul class="menu-content">
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'sponsor']) }}">Sponsor Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'store sponsor type']) }}">Store Sponsor Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'super store sponsor type']) }}">Super Sponsor Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'self purchase type']) }}">Self Purchase Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'performance bonus type']) }}">Performance Bonus Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'leadership bonus type']) }}">Leadership Bonus Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'team development bonus type']) }}">Team Development Bonus</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'royalty bonus type']) }}">Royalty Bonus Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-view', ['type' => 'reward']) }}">Reward Income</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-total-report') }}">Total Report</a>
                    </li>
                    <li>
                        <a class="menu-item" href="{{ route('user-wallet-monthly-report') }}">Income Receipt</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-payout-view') }}">
                    <i class="la la-inr"></i><span class="menu-title">Payout</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-support-view') }}">
                    <i class="ft-aperture"></i><span class="menu-title">Support</span>
                    <span class="badge badge badge-pill badge-warning float-right">New</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('user-kyc-dashboard') }}"><i class="ft-user-check bg-dark text-white"></i><span class="menu-title">KYC Details</span></a>
            </li>
        </ul>
    </div>
    <div class="navigation-background"></div>
</div>