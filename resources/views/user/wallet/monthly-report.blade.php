@extends('user.template.layout')

@section('title')
    Monthly Report
@stop

@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-8 col-12 mb-2">
            <h3 class="content-header-title">
              Monthly Report
            </h3>
        </div>
    </div>
    <div class="content-body">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Month</th>
                                <th>Report</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($months) == 0)
                                <tr>
                                    <td colspan="8" class="text-center">
                                        Right Now, You have no any information.<br>
                                    </td>
                                </tr>
                            @endif
                            @foreach($months as $index => $month)
                                <tr>
                                    <td>{{ \App\Library\Helper::tableIndex($months, $index) }}</td>
                                    <td>{{ $month->month_name }}</td>
                                    <td>
                                        <a href="{{ route('user-wallet-monthly-report-receipt', ['month' => $month->month_name ]) }}" class="btn btn-sm btn-info">View Report</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $months->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop