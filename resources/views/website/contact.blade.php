@extends('website.template.layout')

@section('page-title','Contact Us')

@section('page-content')

    <div class="breadcrumb-area bc_type t3" style="background-image: url(/website-assets/images/parallaxbg2.jpg)">
        <div class="container">
            <h2 class="page-title">Contact Us</h2>
            <ul class="breadcrumb">
                <li><a href="{{ route('website-home') }}">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>
        </div>
    </div>

    <section id="contact" class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-9 item_left">
                    @if (session('errors'))
                        <div class="alert alert-danger">

                            <ul>
                                @foreach (session('errors')->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success" style="border-radius: 3rem;">
                            {{ session('success') }}
                        </div>
                    @endif
                    <form method="post" class="row contact-form">
                        {{csrf_field()}}
                        <div class="col-md-6">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Your Name">
                        </div>
                        <div class="col-md-6">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="col-md-6">
                            <input type="number" name="contact" id="contact"  class="form-control" placeholder="Contact-No">
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="title" id="title" class="form-control" placeholder="Your Subject">
                        </div>
                        <div class="col-md-12">
                            <textarea name="message" class="form-control" id="message" cols="30" rows="5" placeholder="Message"></textarea>
                            <input type="submit" value="Send Message" class="message-sub pull-right btn btn-blue">
                        </div>
                    </form>
                </div>

                <div class="col-md-3 item_right">
                    <div class="address">
                        <h5>CONTACT INFO</h5>
                        <p><i class="fa fa-map-marker"></i>ARASIS HEALTH & BEAUTY LLP   <p>C/O DIPESH GURUNG,DHUKURIA,NEAR DHUKURIA CLUB NEW CHAMTA,MATIGARA,DARJEELING,<br>
                            Darjeeling,West Bengal,734009,India.</p></p>
                        <p><i class="fa fa-phone"></i>Phone: (+91) 0000-0000-00</p>
                        <p><i class="fa fa-envelope"></i><a href="mailto:customercare.arasis@gmail.com">customercare.arasis@gmail.com</a></p>
                    </div>
                </div>
            </div>

        </div>
    </section>

@stop