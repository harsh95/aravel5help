@extends('website.template.layout')

@section('page-title','Faqs')

@section('page-content')

    <div class="breadcrumb-area bc_type t3" style="background-image: url(/website-assets/images/parallaxbg2.jpg)">
        <div class="container">
            <h2 class="page-title">Faqs</h2>
            <ul class="breadcrumb">
                <li><a href="{{ route('website-home') }}">Home</a></li>
                <li class="active">Faqs</li>
            </ul>
        </div>
    </div>
    <section class="">
        <div class="container">
            @if($faqs->count() == 0)
                <div class="row">
                    <div class="col-md-12 text-center">
                        No Faqs Available
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            @foreach($faqs as $index => $faq)
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="heading{{ $index + 1 }}">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $index + 1  }}" aria-expanded="true" aria-controls="collapse{{ $index + 1  }}">
                                                {{ $faq->question }}
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse{{ $index + 1  }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{ $index + 1 }}">
                                        <div class="panel-body">
                                            <p>{{ strip_tags($faq->answer)  }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </section>
@stop
@section('page-javascript')
    <script>
        ( function( $ ) {
            $( document ).ready( function(){
                $('.panel-collapse').removeClass('in');
                $('.panel-title > a').addClass('collapsed');
            });
        })( jQuery );
    </script>
@stop
