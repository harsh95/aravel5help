@extends('website.template.layout')

@section('page-title','Gallery')

@section('page-content')

    <div class="breadcrumb-area bc_type t3" style="background-image: url(/website-assets/images/parallaxbg2.jpg)">
        <div class="container">
            <h2 class="page-title">Gallery</h2>
            <ul class="breadcrumb">
                <li><a href="{{ route('website-home') }}">Home</a></li>
                <li class="active">Gallery</li>
            </ul>
        </div>
    </div>

    <section class="team team-member">
        <div class="container">
            <div class="row staff-filtering">
                <div class="col-md-12">
                    @if (session('error'))
                        <div class="alert d-block alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                </div>
                @foreach($galleries as $index => $gallery)
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center executive">
                        <a href="{{ route('website-gallery-items',['id' => $gallery->id ])  }}">
                            <div class="portfolio-item gallery-items">
                                <div class="portfolio-head">
                                    <img src="{{ $gallery->primary_image ?
                                    env('GALLERY_IMAGE_URL').$gallery->primary_image :
                                    '/website-assets/images/image-not-found.png'  }}"
                                         alt="portfolio" class="img-responsive">
                                    <div class="caption-top">
                                        <a data-lightbox="gallery"
                                           href="{{ $gallery->primary_image ?
                                           env('GALLERY_IMAGE_URL').$gallery->primary_image :
                                           '/website-assets/images/image-not-found.png'  }}">open</a>
                                    </div>
                                </div>
                                <div class="entry-meta">
                                    <a href="{{ route('website-gallery-items',['id' => $gallery->id ])  }}">
                                        <h5 class="gallery-font">
                                            {{ $gallery->name }}
                                        </h5></a>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@stop