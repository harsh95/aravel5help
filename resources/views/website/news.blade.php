@extends('website.template.layout')

@section('page-title','News')

@section('page-content')

    <div class="breadcrumb-area bc_type t3" style="background-image: url(/website-assets/images/parallaxbg2.jpg)">
        <div class="container">
            <h2 class="page-title">News</h2>
            <ul class="breadcrumb">
                <li><a href="{{ route('website-home') }}">Home</a></li>
                <li class="active">News</li>
            </ul>
        </div>
    </div>

    <section class="section">
        <div class="container">
            <div class="card">
                <div class="card-body">
                    @if($news== null)
                        <div class="row">
                            <div class="col-md-12 text-center">
                                No News Available
                            </div>
                        </div>
                    @else
                        <div class="row">
                            <div class="col-md-12 mr-1">
                                @foreach($news as $index => $new)
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading{{ $index + 1 }}">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $index + 1  }}" aria-expanded="true" aria-controls="collapse{{ $index + 1  }}">
                                                    <h4 class="panel-title text-center text-bold-700"> {{ $new->title }}</h4>

                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse{{ $index + 1  }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{ $index + 1 }}">
                                            <div class="panel-body">
                                                <p>{{ strip_tags($new->message)  }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
    </section>
@stop
@section('page-javascript')
    <script>
        ( function( $ ) {
            $( document ).ready( function(){
                $('.panel-collapse').removeClass('in');
                $('.panel-title > a').addClass('collapsed');
            });
        })( jQuery );
    </script>
@stop