<div id="header_canvas">
    <header id="head" class="navbar-default header6 arrow2 sticky-header">
        <div class="container">
            <div class="mega-menu-wrapper no-beaf clearfix">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ route('website-home')  }}">
                        <img src="/user-assets/images/company/logo.png" alt="{{ config('project.company') }}">
                    </a>
                </div>
                <nav class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li> <a href="{{ route('website-home') }}">Home</a> </li>
                        <li>
                            <a href="javascript:void(0);">Company</a>
                            <ul class="sub-menu">
                                <li> <a href="{{route('website-company-about')}}">About-Us</a> </li>
                                <li> <a href="{{route('website-company-legals')}}">Legals</a> </li>
                                <li> <a href="{{route('website-company-vision-mission')}}">Vision Mission</a> </li>
                                <li> <a href="{{route('website-company-terms')}}">Company Terms</a> </li>
                                <li><a href="/website-assets/business_plan/business_plan1.pdf" target="_blank"> Business Plan</a> </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);">Media</a>
                            <ul class="sub-menu">
                                <li> <a href="{{route('website-gallery')}}">Gallery</a> </li>
                                <li><a href="{{ route('website-news')  }}">News</a></li>
                            </ul>
                        </li>
                        <li> <a href="{{ route('website-contact') }}">Contact Us</a> </li>
                        <li><a href="{{route('user-register')}}" target="_blank">Sign Up</a></li>
                        <li><a href="{{route('user-login')}}" target="_blank">Log In</a></li>
                        <li><a href="{{route('store-login')}}" target="_blank">Store Login</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
</div>
