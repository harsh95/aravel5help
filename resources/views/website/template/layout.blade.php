<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="arasis">
    <meta name="keywords" content="arasis">
    <meta name="author" content="arasis"/>
    <meta name="copyright" content="arasis"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="canonical" href="http://arasis.com/"/>
    <meta name="robots" content="index, follow">

    <meta property="og:description" content="arasis">
    <meta property="og:image" content="/user-assets/images/company/logo.png">
    <meta property="og:url" content="http://arasis.com/">
    <meta property="og:title" content="arasis">
    <meta property="og:type" content="arasis">
    <meta property="og:site_name" content="arasis">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Domine:wght@400;500;600;700&display=swap" rel="stylesheet">

    <link rel="icon" type="image/png" href="/user-assets/images/company/favicon.png">
    <title>@yield('page-title') | Welcome To Arasis</title>
    <!-- CSS Files-->



    <!-- CSS ============================================= -->
    <link type="text/css" rel="stylesheet" href="/website-assets/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/jquery.bxslider.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/js/vendor/owl.carousel.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/magnific-popup.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/lightbox.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/icomoon.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/flaticon.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/supersized.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/bootstrap-timepicker.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/jquery-ui.min.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/animate.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/multiscroll.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/selectize.default.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/jquery.fullPage.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/bbpress.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/jquery.timepicker.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/bbp-theme.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/buddypress.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/css/buddypress-theme.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/syntax-highlighter/scripts/prettify.min.css">
    <!-- Shortcodes main stylesheet -->
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/prettyPhoto.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/eislideshow.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/nivo-slider.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/liteaccordion.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/flexslider.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/iconmoon.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/slicebox.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/camera.css">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/main.css?v=2">
    <link type="text/css" rel="stylesheet" href="/website-assets/shortcodes/css/media-queries.css">
    <link rel="stylesheet" href="/website-assets/css/swiper.css" >
    <!-- Main Stylesheet -->
    <link type="text/css" rel="stylesheet" href="/website-assets/css/main.css?v=4">
    <!-- CSS media queries -->
    <link type="text/css" rel="stylesheet" href="/website-assets/css/media-queries.css">
    <!-- End CSS-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />

    @yield('page-css')
    @yield('import-css')
</head>
<body class="bordered">

<div id="preloader">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>

@include('website.template.header')

@yield('page-content')

@include('website.template.footer')

<!-- back to top -->
<a href="javascript:;" id="go-top">
    <i class="fa fa-angle-up"></i>
    top
</a>
<!-- end #go-top -->

<!-- js files -->
<script>
    @if(session('errors'))
        swal('Oops', '{{ session('errors')->first() }}', 'error');
    @elseif(session('error'))
       swal('Oops', '{{ session('error') }}', 'error');
    @elseif(session('success'))
       swal('Hurray', '{{ session('success') }}', 'success');
    @endif
</script>


<script type="text/javascript" src="/website-assets/js/vendor/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="/website-assets/js/vendor/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery-migrate.min.js"></script>
<script type="text/javascript" src="/website-assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.appear.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/website-assets/js/vendor/owl.carousel.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.backstretch.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.nav.js"></script>
<script type="text/javascript" src="/website-assets/js/lightbox.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.parallax-1.1.3.js"></script>
<script type="text/javascript" src="/website-assets/http://maps.googleapis.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="/website-assets/js/jquery-validation.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.easing.min.js"></script>
<script type="text/javascript" src="/website-assets/js/form.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.multiscroll.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery-countTo.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.mb.YTPlayer.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jflickrfeed.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.selectbox-0.2.min.js"></script>
<script type="text/javascript" src="/website-assets/js/tweetie.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.sticky.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.nicescroll.min.js"></script>
<!--
<script type="text/javascript" src="/website-assets/js/okvideo.min.js"></script>
-->
<script src="/website-assets/js/swiper.js"></script>
<script type="text/javascript" src="/website-assets/js/bootstrap-progressbar.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.circliful.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.easypiechart.js"></script>
<script type="text/javascript" src="/website-assets/js/masonry.pkgd.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.tubular.1.0.js"></script>
<script type="text/javascript" src="/website-assets/js/kenburned.min.js"></script>
<script type="text/javascript" src="/website-assets/js/mediaelement-and-player.min.js"></script>
<!-- shortcode scripts -->
<script type="text/javascript" src="/website-assets/shortcodes/js/jquery.nivo.slider.pack.js"></script>
<script type="text/javascript" src="/website-assets/shortcodes/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="/website-assets/shortcodes/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="/website-assets/shortcodes/js/jquery.slicebox.min.js"></script>
<script type="text/javascript" src="/website-assets/shortcodes/js/jquery.eislideshow.js"></script>
<script type="text/javascript" src="/website-assets/shortcodes/js/camera.min.js"></script>
<script type="text/javascript" src="/website-assets/shortcodes/js/jquery.zaccordion.min.js"></script>
<script type="text/javascript" src="/website-assets/shortcodes/js/main.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.prettySocial.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.zoom.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.countdown.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.webticker.min.js"></script>
<script type="text/javascript" src="/website-assets/js/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="/website-assets/js/selectize.min.js"></script>
<!-- image filter -->
<script type="text/javascript" src="/website-assets/js/img-filter/jquery.gray.min.js"></script>
<!-- // image filter -->
<script type="text/javascript" src="/website-assets/js/wow.min.js"></script>
<script src="/website-assets/js/prettify.min.js"></script>
<script type="text/javascript">$.SyntaxHighlighter.init();</script>
<script type="text/javascript" src="/website-assets/js/main.js"></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@yield('page-js')
@yield('import-js')
@yield('page-javascript')
<!-- End js -->
</body>
</html>