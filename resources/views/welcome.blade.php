<!doctype html>
<html lang="zxx">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ingenious - A Direct Selling Software by Tymk Softwares</title>
    <link rel="shortcut icon" href="/website-assets/introduction/img/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="/website-assets/introduction/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">
    <link rel="stylesheet" href="/website-assets/introduction/css/lity.min.css">
    <link rel="stylesheet" href="/website-assets/introduction/css/animations.css">
    <link rel="stylesheet" href="/website-assets/introduction/css/style.css?version=1">
    <link rel="stylesheet" href="/website-assets/introduction/css/media-queries.css">
    <link rel="stylesheet" href="/website-assets/introduction/css/color-theme/color-theme-1.css">
</head>
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="status">
        <div class="spinner">
            <div class="bg-color-2 rect1"></div>
            <div class="bg-color-2 rect2"></div>
            <div class="bg-color-2 rect3"></div>
            <div class="bg-color-2 rect4"></div>
            <div class="bg-color-2 rect5"></div>
        </div>
    </div>
</div>
<!-- Preloader end -->
<div id="top" class="page-wrap header-bg">
    <nav id="navigation" class="navbar navbar-expand-lg">
        <div class="container">
            <!-- Logo -->
            <a class="navbar-brand" href="">
                <div class="logo bg-color-2">Ing</div>
            </a>
            <!-- Logo end -->
            <!-- Burger menu button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="burger-menu">
                            <span class="burger"></span>
                        </span>
            </button>
            <!-- Burger menu button end -->
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <!-- Navigation Link 1 -->
                    <li class="nav-item active">
                        <a class="nav-link mx-auto" href="#top">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#hiw">How It Works</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://mlmproducts.co/mlm-software/features" target="_blank">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://mlmproducts.co/">Contact</a>
                    </li>
                    <li class="nav-item button header-button bg-color-2">
                        <a class="nav-link" href="http://mlmproducts.co/"><i class="fas fa-cart-arrow-down"></i> Purchase now</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <header class="header">
        <div class="container">
            <div class="row header-row">
                <!-- Main title -->
                <div class="col-sm-12 col-md-6 align-self-center main-title">
                    <p class="light bold small main-suptitle">Most Advance & Clever</p>
                    <h1><b>Ingenious</b><span class="thin"> - Created To Make Direct Selling Easy</span></h1>
                    <!-- Buttons -->
                    <a href="/admin-panel">
                        <div class="button bg-color-2">Explore Admin</div>
                    </a>
                    <a class="gray link" href="/user">Visit User Panel <i class="fas fa-chevron-right"></i></a>
                </div>
                <!-- Main title end -->
                <div class="col-12 col-md-6 align-self-center header-macbook">
                    <img class="jump" src="/website-assets/introduction/img/welcome-screen.svg" alt="macbook">
                </div>
            </div>
            <!-- Tooltip -->
            <a href="#hiw">
                <div class="header-tooltip"><i class="fas scroll-arrow bounce fa-angle-double-down"></i>
                    <p class="light thin small">Scroll Down<br>To Learn More</p>
                </div>
            </a>
            <!-- Tooltip end -->
        </div>
    </header>
    <!-- Header end -->
    <!-- How it works -->
    <section id="hiw" class="hiw p-90-60">
        <div class="line"></div>
        <div class="container">
            <!-- Section title -->
            <div class="section-title sm-ac mb-90">
                <p class="light bold small">Build for People</p>
                <h2><span class="thin">How it</span> <b>Works</b></h2>
                <p class="light thin">Simple Steps & Advance System</p>
            </div>
            <!-- Section title end -->
            <div class="row">
                <!-- Item 1 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: 0s">1</div>
                    <p class="bold">Most Advance</p>
                    <p class="light thin">Big Data management, Cloud Computing, Complex Downline Manager</p>
                </div>
                <!-- Item 2 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: .75s">2</div>
                    <p class="bold">Choose plan</p>
                    <p class="light thin">Your Business Plan & Our Ingenious System, choose any add on as per your requirement </p>
                </div>
                <!-- Item 3 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: 1.5s">3</div>
                    <p class="bold">Highly Secured</p>
                    <p class="light thin">Ingenious Software is provided with premium security features which can save you from any security issues</p>
                </div>
                <!-- Item 4 -->
                <div class="col-6 col-sm-6 col-md-3 hiw-item mb-30">
                    <div class="bg-color-2 puls mb-30" style="animation-delay: 2.25s">4</div>
                    <p class="bold">Super Crafted!</p>
                    <p class="light thin">Crafted with Laravel, VueJs, Nested sets Concept, Material Design, AWS S3, Postgresql & many more</p>
                </div>
            </div>
        </div>
    </section>
    {{-- Footer --}}
    <footer id="footer" class="bg-animation p-90-60">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-3 align-self-center mb-30">
                    <!-- Logo -->
                    <a class="footer-brand" href="#">
                        <div class="logo bg-color-1">Ing</div>
                    </a>
                    <!-- Logo end -->
                </div>
                <div class="col-12 col-md-9 align-self-center justify-content-center mb-30">
                    <ul class="footer-nav">
                        <li class="nav-item active">
                            <a class="nav-link mx-auto" target="_blank" href="http://mlmproducts.co/">Purchase</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin-panel">Admin Panel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/user">User Panel</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <div class="footer-copyright">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-12 col-sm-6">
                    <!-- Copyright -->
                    <p class="small thin white-light sm-ac">Copyrights © {{ date('Y') }}. All Rights Reserved.</p>
                </div>
                <div class="col-12 col-sm-6">
                    <!-- Author -->
                    <p class="small thin white-light author sm-ac">Designed & Developed by <a href="http://tymksoftwares.in/">Tymk Softwares</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="/website-assets/introduction/js/bootstrap.min.js"></script>
<script src="/website-assets/introduction/js/smooth-scroll.min.js"></script>
<script src="/website-assets/introduction/js/lity.js"></script>
<script src="/website-assets/introduction/js/main.js"></script>
</body>
</html>